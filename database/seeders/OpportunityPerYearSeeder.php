<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OpportunityPerYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('opportunity_per_year_i18n')->truncate();
        DB::table('opportunity_per_year')->truncate();
        DB::table('opportunity_per_year')->insert([
            [
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('opportunity_per_year_i18n')->insert([
            [
                'opportunity_per_year_id' => '1',
                'title'=>'I have never organized a program',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '1',
                'title'=>'لم أقم بتنظيم برنامج من قبل',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '2',
                'title'=>'Up to 3 programs per year',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '2',
                'title'=>'ما يصل إلى 3 برامج في السنة',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '3',
                'title'=>'More than 3 programs per year',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '3',
                'title'=>'أكثر من 3 برامج في السنة',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '4',
                'title'=>'More than 10 programs per year',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '4',
                'title'=>'أكثر من 10 برامج في السنة',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '5',
                'title'=>'More than 20 programs per year',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'opportunity_per_year_id' => '5',
                'title'=>'أكثر من 20 برنامجًا سنويًا',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
    }
}
