<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('pages_sponsor_showcase')->truncate();
        DB::table('pages_sponsor_whatsinit')->truncate();
        DB::table('pages_sponsor_seo')->truncate();
        DB::table('pages_contactus_pageinfo')->truncate();
        DB::table('pages_contactus_seo')->truncate();
        DB::table('pages_privacy_policy_page_info')->truncate();
        DB::table('pages_privacy_policy_seo')->truncate();
        DB::table('pages_terms_condition_page_info')->truncate();
        DB::table('pages_terms_condition_seo')->truncate();
        DB::table('pages_about_us_page_info')->truncate();
        DB::table('pages_about_us_seo')->truncate();
        DB::table('pages')->truncate();
        DB::table('pages')->insert([
            [
                'template' => 1,
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'template' => 2,
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'template' => 2,
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'template' => 4,
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'template' => 5,
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'template' => 3,
                'created_at' => now(),
                'updated_at' => now()
            
            ],
           
        ]);
        DB::table('pages_about_us_page_info')->insert([
            [
                'pages_id' => '1',
                'title'=>'About Us',
                'content'=>'Lorem Ipsum is simply dummy text',
                'image'=>'',
                'sub_title'=>'Lorem Ipsum is simply dummy text',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'sub_content'=>'Lorem Ipsum is simply dummy text',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '1',
                'title'=>'About Us',
                'content'=>'Lorem Ipsum is simply dummy text',
                'image'=>'',
                'sub_title'=>'Lorem Ipsum is simply dummy text',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'sub_content'=>'Lorem Ipsum is simply dummy text',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_about_us_seo')->insert([
            [
                'pages_id' => '1',
                'meta_title'=>'About Us',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '1',
                'meta_title'=>'About Us',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_terms_condition_page_info')->insert([
            [
                'pages_id' => '2',
                'title'=>'Terms & Condition',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '2',
                'title'=>'Terms & Condition',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_terms_condition_seo')->insert([
            [
                'pages_id' => '2',
                'meta_title'=>'Terms & Condition',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '2',
                'meta_title'=>'Terms & Condition',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
        DB::table('pages_privacy_policy_page_info')->insert([
            [
                'pages_id' => '3',
                'title'=>'Privacy policy',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '3',
                'title'=>'Privacy policy',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_privacy_policy_seo')->insert([
            [
                'pages_id' => '3',
                'meta_title'=>'Privacy policy',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '3',
                'meta_title'=>'Privacy policy',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
        DB::table('pages_contactus_pageinfo')->insert([
            [
                'pages_id' => '4',
                'title'=>'Contact Us',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'address'=>'Address',
                'email'=>'contact@reayaat.com',
                'phone'=>'98771234',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '4',
                'title'=>'Contact Us',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'address'=>'Address',
                'email'=>NULL,
                'phone'=>NULL,
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_contactus_seo')->insert([
            [
                'pages_id' => '4',
                'meta_title'=>'Contact Us',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '4',
                'meta_title'=>'Contact Us',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
        DB::table('pages_sponsor_showcase')->insert([
            [
                'pages_id' => '5',
                'title'=>'Looking for a sponsor',
                'step1_title'=>'Submit your content',
                'step1_desc'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'step2_title'=>'Communicate with sponsors',
                'step2_desc'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'step3_title'=>'Get deals done!',
                'step3_desc'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '5',
                'title'=>'Looking for a sponsor',
                'step1_title'=>'Submit your content',
                'step1_desc'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'step2_title'=>'Communicate with sponsors',
                'step2_desc'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'step3_title'=>'Get deals done!',
                'step3_desc'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_sponsor_whatsinit')->insert([
            [
                'pages_id' => '5',
                'title'=>'What In Its For You',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_title1'=>'Create your profile',
                'sub_content1'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image1' => NULL,
                'sub_title2'=>'Choose your subscription plan',
                'sub_content2'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image2' => NULL,
                'sub_title3'=>'Connect with sponsors',
                'sub_content3'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image3' => NULL,
                'sub_title4'=>'Lock your deals!',
                'sub_content4'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image4' => NULL,
                
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '5',
                'title'=>'What In Its For You',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_title1'=>'Create your profile',
                'sub_content1'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image1' => NULL,
                'sub_title2'=>'Choose your subscription plan',
                'sub_content2'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image2' => NULL,
                'sub_title3'=>'Connect with sponsors',
                'sub_content3'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image3' => NULL,
                'sub_title4'=>'Lock your deals!',
                'sub_content4'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'sub_image4' => NULL,
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_sponsor_seo')->insert([
            [
                'pages_id' => '5',
                'meta_title'=>'Looking for a sponsor?',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '5',
                'meta_title'=>'Looking for a sponsor?',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
        DB::table('pages_home_pageinfo')->insert([
            [
                'pages_id' => '6',
                'title'=>'How it works',
                'left_title'=>'I am looking for an opportunity to sponsor',
                'left_desc'=>'Discover opportunities and get in contact with the content owner instantly',
                'left_subtitle'=>'Lets Discover',
                'right_title'=>'I am organizing a program',
                'right_desc'=>'Create your profile and showcase to sponsors what opportunities you have available',
                'right_subtitle'=>'Lets Create',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '6',
                'title'=>'How it works',
                'left_title'=>'I am looking for an opportunity to sponsor',
                'left_desc'=>'Discover opportunities and get in contact with the content owner instantly',
                'left_subtitle'=>'Lets Discover',
                'right_title'=>'I am organizing a program',
                'right_desc'=>'Create your profile and showcase to sponsors what opportunities you have available',
                'right_subtitle'=>'Lets Create',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_home_seo')->insert([
            [
                'pages_id' => '6',
                'meta_title'=>'Home',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'pages_id' => '6',
                'meta_title'=>'Home',
                'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
