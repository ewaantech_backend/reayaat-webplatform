<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            [
                'name' => 'Super Admin ',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Admin ',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
