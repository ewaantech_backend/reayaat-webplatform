<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->text('slide_id');
            $table->enum('status', ['active', 'deactive']);
            $table->text('title_en')->nullable();
            $table->text('title_ar')->nullable();
            $table->text('read_more_link_en')->nullable();
            $table->text('read_more_link_ar')->nullable();
            $table->text('content_en')->nullable();
            $table->text('content_ar')->nullable();
            $table->text('path_en')->nullable();
            $table->text('path_ar')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slide');
    }
}
