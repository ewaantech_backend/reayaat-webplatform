<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_settings', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->text('currency')->nullable();
            $table->integer('notify_days_monthly_pkg')->nullable();
            $table->integer('notify_days_yearly_pkg')->nullable();
            $table->enum('opportunity_per_year_display',['enable', 'disable'])->default('disable');
            $table->enum('testimonial_display',['enable', 'disable'])->default('disable');
            $table->enum('sponsor_display',['enable', 'disable'])->default('disable');
            $table->enum('total_event_profile_viewed_display',['enable', 'disable'])->default('disable');
            $table->enum('registered_views_display',['enable', 'disable'])->default('disable');
            $table->enum('opportunities_display',['enable', 'disable'])->default('disable');
            $table->enum('connection_mode_display',['enable', 'disable'])->default('disable');
            $table->text('facebook_link')->nullable();
            $table->text('linkedin_link')->nullable();
            $table->text('instagram_link')->nullable();
            $table->text('twitter_link')->nullable();
            $table->text('copyright_tag')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_settings');
    }
}
