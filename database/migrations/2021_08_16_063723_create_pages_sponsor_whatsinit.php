<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesSponsorWhatsinit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages_sponsor_whatsinit', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('pages_id');
            $table->text('title');
            $table->text('content');
            $table->text('sub_title1');
            $table->text('sub_title2');
            $table->text('sub_title3');
            $table->text('sub_title4');
            $table->text('sub_content1');
            $table->text('sub_content2');
            $table->text('sub_content3');
            $table->text('sub_content4');
            $table->text('sub_image1')->nullable();
            $table->text('sub_image2')->nullable();
            $table->text('sub_image3')->nullable();
            $table->text('sub_image4')->nullable();
            $table->enum('language', ['en', 'ar']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pages_id')
                ->references('id')
                ->on('pages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_sponsor_whatsinit');
    }
}
