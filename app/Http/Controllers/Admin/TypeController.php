<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\ActivityLog;
use App\Models\Type;
use App\Models\TypeLang;
use App\Models\Category;

class TypeController extends Controller
{
    use ActivityLog ;
    public function get(Request $request)
    {
        $search_field=$request->search_field ?? '';
        
        if($search_field)
        {
            $data=TypeLang::select('type_id')->where('name', 'like', "%" . $search_field . "%")
            ->get()->toArray();
            $da=[];
            foreach($data as  $d)
            {
                $da[]=$d['type_id'];
            }
            $type= Type::with('lang')->whereIn('id',$da)->paginate(20);
        
        }else{
            $type=Type::with('lang')->paginate(20);
        }

        $category =Category::with('lang')->where('status','active')->get();
        $parent_type =Type::with('lang')->where('status','active')->where('is_parent','yes')->get();
        return view('admin.settings.type.index',compact('type','category','parent_type','search_field'));
    }
    public function store(Request $request)
    {
        $type_id = $request->type_id ;
        if ($type_id) {
            $unique = ',' . $request->type_id;
        } else {
            $unique = ',NULL';
        }
        $rules = [
            'type_name_en' => 'required|unique:type_i18n,name' . $unique . ',type_id,language,en,deleted_at,NULL',
            'type_name_ar' => 'required',
            'category' => 'required',
        ];
        $messages = [
            'type_name_en.unique' => 'Type already exist.',
            'type_name_en.required' => 'Type (EN) is required.',
            'type_name_ar.required' => 'Type (AR) is required.',
            'category' => 'Categoey required',
        ];

        if($type_id)
        {
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {

                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                $parent_id = $request->parent_type ?? NULL;
                $is_parent = $request->parent_type ? 'no' : 'yes';

                Type::where('id', $request->type_id)
                        ->update([
                    'category_id' => $request->category,
                    'parent_id' => $parent_id,
                    'is_parent' => $is_parent
                ]);

                TypeLang::where('type_id', $request->type_id)->where('language', 'en')
                        ->update([
                    'name' => $request->type_name_en,
                ]);
                TypeLang::where('type_id', $request->type_id)->where('language', 'ar')
                        ->update([
                    'name' => $request->type_name_ar,
                ]);
                $msg = "Type updated successfully";
                return response()->json(['status' => 1, 'message' => $msg]);
               
            }
            
        }else{
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {
                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                $parent_id = $request->parent_type ?? NULL;
                $is_parent = $request->parent_type ? 'no' : 'yes';

                $type_data = Type::create([
                    'status' => "deactive",
                    'category_id' => $request->category,
                    'parent_id' => $parent_id,
                    'is_parent' => $is_parent
                        
                ]);
                $type_data->lang()->createMany([
                    [
                        'name' => $request->type_name_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->type_name_ar,
                        'language' => 'ar',
                    ],
                ]);
                if($type_data)
                {
                    $this->updateActivity();
                    return response()->json(['status' => 1, 'message' => 'Type created successfully']);
                }else{
                    return response()->json(['status' => 2, 'message' => 'Sorry something went wrong.']);
                }
                
            }
        }
    }

    public function edit($id)
    {
        $type=Type::with('lang')->where('id',$id)->first();
        $sub_type=Type::where('parent_id',$id)->count();
        return[
            'type'=>$type,
            'sub_type'=>$sub_type
        ];
    }
    public function destroy(Request $req){
        $type = Type::find($req->id);
        $sub_type=Type::where('parent_id',$req->id)->count();
        if($type){
            if($sub_type > 0){
                return response()->json(['status' => 0, 'message' => 'Cannot be deleted, have related records']);
            }else{
                $type->lang()->delete();
                $type->delete();
                $this->updateActivity();
                return response()->json(['status' => 1, 'message' => 'Type deleted successfully']);
            }
            
        }else{
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }
    public function updateStatus(Request $req)
    {
        $att = Type::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Type::where('id', $req->id)
                        ->update([
                            'status' => 'active'
                ]);
            } else {
                Type::where('id', $req->id)
                        ->update([
                            'status' => 'deactive'
                ]);
            }
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
