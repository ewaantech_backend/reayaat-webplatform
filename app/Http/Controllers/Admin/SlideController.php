<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slide;
use Validator;
use App\Traits\ActivityLog;

class SlideController extends Controller
{
    use ActivityLog ;
    public function index(Request $req) {
        
        $data = array();
        $slide_data = Slide::paginate(10);
        $data = [
            'slider_data' => $slide_data,
        ];
        return view('admin.cms.slide.slide', $data);
    }
    
    public function create(Request $req) {
      $slide_id = $req->id;
      $row_data = array();
        if ($slide_id != '') {
            $row_data = Slide::where('id', '=', $slide_id)->first();
        }
      return view('admin.cms.slide.create', compact('row_data'));  
    }
    
    public function upload_image(Request $request) {
        $rules = [
            'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'photo.max' => 'The image must be less than 2Mb in size',
            'photo.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = request()->file('photo');
            $path = $file->store("slide", ['disk' => 'public_uploads']);
            if($request->sd_id){
                Slide::where('id', $request->sd_id)
                        ->update([
                        "{$request->type}" => $path
                ]);
            }else{
                $slide_created = Slide::create([
                        'slide_id' => $this->generateSlideId(),
                        "{$request->type}" => $path
                ]);
            }
           
            $sd_id = $request->sd_id ? $request->sd_id : $slide_created->id;
            $full_path = url('uploads/' . $path);
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'File uploaded successfully', 'path' => $full_path, 'sd_id' => $sd_id, 'type' => $request->type]);
        }
    }

    public function detete_img(Request $request) {
        $slide = Slide::where('id', $request->id)->first();
        $type = $request->type;
        Slide::where('id', $request->id)
                ->update([
                    $type => ''
        ]);
        if (!empty($slide)) {
            $file_path = public_path() . '/uploads/' . $slide[$type];
            if(is_file($file_path))
            unlink($file_path);
        }
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => 'File deleted successfully', 'id' => $request->id, 'type' => $request->type]);
    }

    public function save(Request $request) {

        $slide_id = $request->slide_id ;
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'content_en' => 'required',
            'content_ar' => 'required',
        ];
        $messages = [
            'title_en.required' => 'Title(EN) is required.',
            'title_ar.required' => 'Title(AR) is required.',
            'content_en.required' => 'Content(EN) is required.',
            'content_ar.required' => 'Content(AR) is required.',
        ];
        if($slide_id)
        {
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {

                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                Slide::where('id', $slide_id)
                        ->update([
                    'title_en' => $request->title_en,
                    'read_more_link_en' => $request->read_more_link_en ?? '',
                    'content_en' => $request->content_en,
                    'title_ar' => $request->title_ar,
                    'read_more_link_ar' => $request->read_more_link_ar ?? '',
                    'content_ar' => $request->content_ar,
                ]);
               
                $msg = "Slide updated successfully";
                return response()->json(['status' => 1, 'message' => $msg]);
               
            }
            
        }else{
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {
                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                $slide_data = Slide::create([
                    'status' => "deactive",
                    'slide_id' => $this->generateSlideId(),
                    'title_en' => $request->title_en,
                    'read_more_link_en' => $request->read_more_link_en ?? '',
                    'content_en' => $request->content_en,
                    'title_ar' => $request->title_ar,
                    'read_more_link_ar' => $request->read_more_link_ar ?? '',
                    'content_ar' => $request->content_ar,
                        
                ]);
               
                if($slide_data)
                {
                    $this->updateActivity();
                    return response()->json(['status' => 1, 'message' => 'Slide created successfully']);
                }else{
                    return response()->json(['status' => 2, 'message' => 'Sorry something went wrong.']);
                }
                
            }
        }
    }
    
    public function changeStatus(Request $req)
    {
        $att = Slide::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Slide::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                Slide::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
            }
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    protected function generateSlideId()
    {
        $lastSlide= Slide::select('id')
                ->orderBy('id', 'desc')
                ->first();
        $lastId = 0;
        
        if ($lastSlide) {
            $lastId = $lastSlide->id;
        }
        $lastId++;
        return 'SL' . str_pad($lastId, 3, '00', STR_PAD_LEFT);
    }

    public function destroy(Request $req)
    {
        $slide=slide::find($req->id);
            $slide->delete();
            $this->updateActivity();
         return response()->json(['status' => 1, 'message' => 'Slide deleted successfully']);
    }
}
