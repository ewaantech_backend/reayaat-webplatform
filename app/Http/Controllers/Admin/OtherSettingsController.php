<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\OtherSettings;
use App\Traits\ActivityLog;

class OtherSettingsController extends Controller
{
    use ActivityLog ;
    public function get()
    {
        $other = OtherSettings::first();

        return view('admin.settings.other.index',compact('other'));
    }
    public function store(Request $request)
    {
        //dd($request->toArray());
        $other = OtherSettings::first();
        $data = [
            'currency' => json_encode([
                'currency_en' => $request->currency_en,
                'currency_ar' => $request->currency_ar
            ]),
            'notify_days_monthly_pkg' => $request->notify_days_monthly_pkg,
            'notify_days_yearly_pkg' => $request->notify_days_yearly_pkg,

            'opportunity_per_year_display'=> $request->opportunity_per_year_display =='Y' ? "enable" :"disable",
            'registered_views_display'=> $request->registered_views_display =='Y' ? "enable" :"disable",
            'testimonial_display'=> $request->testimonial_display =='Y' ? "enable" :"disable",
            'opportunities_display'=> $request->opportunities_display =='Y' ? "enable" :"disable",
            'sponsor_display'=> $request->sponsor_display =='Y' ? "enable" :"disable",
            'connection_mode_display'=> $request->connection_mode_display =='Y' ? "enable" :"disable",
            'total_event_profile_viewed_display'=> $request->total_event_profile_viewed_display =='Y' ? "enable" :"disable",
            
            'facebook_link' => $request->facebook_link,
            'linkedin_link' => $request->linkedin_link,
            'instagram_link'=> $request->instagram_link,
            'twitter_link'=> $request->twitter_link,
            'copyright_tag'=> $request->copyright_tag,

        ];
        if (!empty($other)) {
            $setting_save = OtherSettings::where('id', $other->id)->update(
                $data
            );
            $this->updateActivity();
            Session::flash('success','Other Settings Updated Successfully');
            return redirect()->route('admin.other.get');
            // ->with('success', 'Other Settings Updated Successfully');
        } else {
            $setting_save = OtherSettings::Create(
                $data
            );
            $this->updateActivity();
            Session::flash('success','Other setting Added Successfully');
            return redirect()->route('admin.other.get');
            // ->with('success', 'Other setting Added Successfully');
        }
    }
}
