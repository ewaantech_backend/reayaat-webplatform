<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\ActivityLog;
use App\Models\Category;
use App\Models\CategoryLang;
use App\Models\Type;
use App\Models\Topic;

class CategoryController extends Controller
{
    use ActivityLog ;
    public function get()
    {
        $category=Category::with('lang') ->paginate(20);
        return view('admin.settings.category.index',compact('category'));
    }
    public function store(Request $request)
    {
        $category_id = $request->category_id ;
        if ($category_id) {
            $unique = ',' . $request->category_id;
        } else {
            $unique = ',NULL';
        }
        $rules = [
            'cat_name_en' => 'required|unique:category_i18n,name' . $unique . ',category_id,language,en,deleted_at,NULL',
            'cat_name_ar' => 'required',
        ];
        $messages = [
            'cat_name_en.unique' => 'Category already exist.',
            'cat_name_en.required' => 'category_name_en is required.',
            'cat_name_ar.required' => 'category_name_ar is required.',
        ];

        if($category_id)
        {
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {

                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                CategoryLang::where('category_id', $request->category_id)->where('language', 'en')
                        ->update([
                    'name' => $request->cat_name_en,
                ]);
                CategoryLang::where('category_id', $request->category_id)->where('language', 'ar')
                        ->update([
                    'name' => $request->cat_name_ar,
                ]);
                $msg = "Category updated successfully";
                return response()->json(['status' => 1, 'message' => $msg]);
               
            }
            
        }else{
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {
                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {
               $category_data = Category::create([
                        'status' => "deactive"
                        
                ]);
                $category_data->lang()->createMany([
                    [
                        'name' => $request->cat_name_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->cat_name_ar,
                        'language' => 'ar',
                    ],
                ]);
                if($category_data)
                {
                    $this->updateActivity();
                    return response()->json(['status' => 1, 'message' => 'Category created successfully']);
                }else{
                    return response()->json(['status' => 2, 'message' => 'Sorry something went wrong.']);
                }
                
            }
        }
    }

    public function edit($id)
    {
        $category=Category::with('lang')->where('id',$id)->first();
        return[
            'category'=>$category
        ];
    }
    public function destroy(Request $req){
        $category = Category::find($req->id);
        $type_count = Type::where('category_id', $req->id)->count();
        $topic_count = Topic::where('category_id', $req->id)->count();

        if($category){
            if($type_count > 0 || $topic_count > 0){
                return response()->json(['status' => 0, 'message' => 'Cannot be deleted, have related records']);
            }else{
                $category->lang()->delete();
                $category->delete();
                $this->updateActivity();
                return response()->json(['status' => 1, 'message' => 'Category deleted successfully']);
            }
            
        }else{
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }
    public function updateStatus(Request $req)
    {
        $att = Category::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Category::where('id', $req->id)
                        ->update([
                            'status' => 'active'
                ]);
            } else {
                Category::where('id', $req->id)
                        ->update([
                            'status' => 'deactive'
                ]);
            }
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
