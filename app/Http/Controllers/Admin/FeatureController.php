<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\ActivityLog;
use App\Models\Feature;
use App\Models\FeatureLang;

class FeatureController extends Controller
{
    use ActivityLog ;
    public function get()
    {
        $feature=Feature::with('lang') ->paginate(20);
        return view('admin.packages.features.index',compact('feature'));
    }

    public function edit($id)
    {
        $feature=Feature::with('lang')
                ->where('id',$id)->first();       
        return[
            'feature'=>$feature
        ];
    }
    public function update(Request $request)
    {
       $sement= DB::transaction(function () use ($request ) {
       
            FeatureLang::where('language','en')
            ->where('feature_id',$request->id)
            ->update([
                'title'=>$request->title_en,
                'description'=>$request->content_en
            ]);
            FeatureLang::where('language','ar')
            ->where('feature_id',$request->id)
            ->update([
                'title'=>$request->title_ar,
                'description'=>$request->content_ar
            ]);
            
        });
        $this->updateActivity();
        return [
            "msg"=>"success"

        ];

    }

    public function statusUpdate(Request $request)
    {
      
        $status=$request->status === 'Deactivate' ? 'deactive' : 'active' ;
        Feature::where('id',$request->id)
             ->update([
                'status'=>$status
        ]);
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
}
