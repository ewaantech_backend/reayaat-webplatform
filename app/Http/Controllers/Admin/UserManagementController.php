<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRoles;
use App\Services\SendEmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\ActivityLog;

class UserManagementController extends Controller
{
    use ActivityLog ;
    protected $user ;

    public function __construct(User $user)
    {
        $this->user = $user ;
    }
    public function get(Request $req)
    {
        $query = $this->user->with('roles');
        $user = $query->where('id','!=',1)->orderBy('name','asc')->paginate(20);
        
        $logged_user = $this->user->where('id','!=', Auth::user()->id)->first();
        $roles =UserRoles::get();
        return view('admin.user.index',compact('user','logged_user','roles')); 
                
    }
    public function store(Request $req)
    {
        $rules = [
            'user_name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'role' => 'required',

        ];
        $messages = [
            'user_name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'User with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'User with same phone number already exists',
            'role.required' => 'Role is required.'
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }else{
            $user=$this->generateUserId();
            $userData= User::
                 create([
                 'name'=>$req->user_name,
                 'email'=>$req->email,
                 'country_code'=>"+966",
                 'phone'=>(int)$req->phone,
                 'role_id'=>$req->role,
                'password'=>Hash::make('1234'),
                 'user_id'=>$user,
                 'status'=>'active',
             ]);
             $msg="User added successfully";
             if ($userData) {
                $this->updateActivity();
                 return response()->json(['status' => 1, 'message' => $msg]);
             } else {
                 return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
             }
        }
    }

    public function updateStatus(Request $req)
    {  
        $status=$req->status === 'Activate' ? 'active':'deactive';
        $statusUpdate=User::where('id',$req->id)
        ->update([
            'status'=>$status
        ]);
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
    public function edit($id)
    {
        $users=User::where('id',$id)->first();
        return[
            'user'=>$users
        ];
    }
    public function update(Request $req)
    {
        $rules = [
            'user_name' => 'required',
            'email' => 'required|unique:users,email,'.$req->user_unique,
            'phone' => 'required|unique:users,phone,'.$req->user_unique,
            'role' => 'required',

        ];
        $messages = [
            'user_name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'User with same email already exists',
            'mobile.required' => 'Phone number is required.',
            'mobile.unique' => 'Phone number should be unique',
            'role.required' => 'Role is required.'
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }else{
            $userData= User::where('id',$req->user_unique)
            ->update([
                'name'=>$req->user_name,
                'email'=>$req->email,
                'country_code'=>"+966",  
                'phone'=>(int)$req->phone,
                'role_id'=>$req->role,
                
            ]);
            $msg="User details updated successfully";
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => $msg]);

            }
    }
    public function destroy(Request $req)
    {
        $user=User::find($req->id);
        $user->delete();
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => 'User deleted successfully']);
       
    }
    protected function generateUserId()
    {
        $lastUser= User::select('id')
                ->orderBy('id', 'desc')
                ->first();
        $lastId = 0;
        
        if ($lastUser) {
            $lastId = $lastUser->id;
        }
        $lastId++;
        return 'REAY-U-' . str_pad($lastId, 4, '100', STR_PAD_LEFT);
    }

    public function sendPassword(Request $req)
    {
            $user=User::where('id',$req->id)->first();
            $password=mt_rand(100000,999999); 
            $data=[
                'password'=>$password,
                'user'=>$user
            ];
            DB::transaction(function () use ($data,$password,$user,$req) {
                User::where('id',$req->id)->update([
                    'password'=>Hash::make($password)
                ]);
            $subject = 'Welcome to REAYAAT !';
            $url = URL::to('/login');
            SendEmail::generateEmail($user->email, $password, $user->name, $subject, 'user', $url);
            });
            $this->updateActivity();
             return response()->json(['status' => 1, 'message' => 'Users credentials sent successfully']);
    }
}
