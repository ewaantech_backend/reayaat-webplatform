<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Events\SendToken;
use App\Models\User;

class AdminVerificationController extends Controller
{
    public function resetPasswordToken(Request $request)
    {
        $email=encrypt($request->email);
        $user_email=User::where('email',$request->email)->first();
               if($user_email){
            $var=mt_rand(1000,9999); 
            // $var = '1234';
            $user=User::where('email',$request->email)->update([
                'verify_token'=>$var,
            ]);
    
             event(new SendToken($request->email, $var));

                Session::flash('success','Verification token send successfully.');
                return view('auth.passwords.reset2',compact('email'));
            }
        else 

        return Redirect::back()->withErrors([ "We can't find a user with that e-mail address"]);
    }

    public function tokenVerify(Request $request)
    {
         
          $email1=decrypt($request->email);
          $email=$request->email;
      
          $user=User::where('email',$email1)->where('verify_token',$request->code)->first();
          
          if($user)
          {
              Session::flash('success','Token Verified Successfully.');
              return view('auth.passwords.reset',compact('email'));
          
          }
          else{
              Session::flash('error','Enter valid token');
              return view('auth.passwords.reset2',compact('email'));
              
          }
    }
    public function passwordUpdating(Request $request)
    {
        
            $email= decrypt($request->email);
            $user=User::where('email',$email)->update(
                [
                    'password' => Hash::make($request->password),
                ]);
                Session::flash('success','Password reset successfully');
                return redirect()->route('login');
        
       

    }
    public function logout(Request $request)
    {

        Auth::guard('web')->logout();
        Session::flash('success','Logout successfully.');
        return redirect()->route('login');
    }
}
