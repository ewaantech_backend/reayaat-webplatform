<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\ActivityLog;
use App\Models\Topic;
use App\Models\TopicLang;
use App\Models\Category;

class TopicController extends Controller
{
    use ActivityLog ;
    public function get(Request $request)
    {
        $search_field=$request->search_field ?? '';
        
        if($search_field)
        {
            $data=TopicLang::select('topic_id')->where('name', 'like', "%" . $search_field . "%")
            ->get()->toArray();
            $da=[];
            foreach($data as  $d)
            {
                $da[]=$d['topic_id'];
            }
            $topic= Topic::with('lang')->whereIn('id',$da)->paginate(20);
        
        }else{
            $topic=Topic::with('lang')->paginate(20);
        }

        $category =Category::with('lang')->where('status','active')->get();
        $parent_topic =Topic::with('lang')->where('status','active')->where('is_parent','yes')->get();
        
        return view('admin.settings.topic.index',compact('topic','category','parent_topic','search_field'));
    }
    public function store(Request $request)
    {
        $topic_id = $request->topic_id ;
        if ($topic_id) {
            $unique = ',' . $request->topic_id;
        } else {
            $unique = ',NULL';
        }
        $rules = [
            'topic_name_en' => 'required|unique:topic_i18n,name' . $unique . ',topic_id,language,en,deleted_at,NULL',
            'topic_name_ar' => 'required',
            'category' => 'required',
        ];
        $messages = [
            'topic_name_en.unique' => 'Topic already exist.',
            'topic_name_en.required' => 'Topic (EN) is required.',
            'topic_name_ar.required' => 'Topic (AR) is required.',
            'category' => 'Categoey required',
        ];

        if($topic_id)
        {
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {

                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                $parent_id = $request->parent_topic ?? NULL;
                $is_parent = $request->parent_topic ? 'no' : 'yes';

                Topic::where('id', $request->topic_id)
                        ->update([
                    'category_id' => $request->category,
                    'parent_id' => $parent_id,
                    'is_parent' => $is_parent
                ]);

                TopicLang::where('topic_id', $request->topic_id)->where('language', 'en')
                        ->update([
                    'name' => $request->topic_name_en,
                ]);
                TopicLang::where('topic_id', $request->topic_id)->where('language', 'ar')
                        ->update([
                    'name' => $request->topic_name_ar,
                ]);
                $msg = "Topic updated successfully";
                return response()->json(['status' => 1, 'message' => $msg]);
               
            }
            
        }else{
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {
                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                $parent_id = $request->parent_topic ?? NULL;
                $is_parent = $request->parent_topic ? 'no' : 'yes';

                $type_data = Topic::create([
                    'status' => "deactive",
                    'category_id' => $request->category,
                    'parent_id' => $parent_id,
                    'is_parent' => $is_parent
                        
                ]);
                $type_data->lang()->createMany([
                    [
                        'name' => $request->topic_name_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->topic_name_ar,
                        'language' => 'ar',
                    ],
                ]);
                if($type_data)
                {
                    $this->updateActivity();
                    return response()->json(['status' => 1, 'message' => 'Topic created successfully']);
                }else{
                    return response()->json(['status' => 2, 'message' => 'Sorry something went wrong.']);
                }
                
            }
        }
    }

    public function edit($id)
    {
        $topic=Topic::with('lang')->where('id',$id)->first();
        $sub_topic=Topic::where('parent_id',$id)->count();
        return[
            'topic'=>$topic,
            'sub_topic'=>$sub_topic
        ];
    }
    public function destroy(Request $req){
        $type = Topic::find($req->id);
        $sub_type=Topic::where('parent_id',$req->id)->count();
        if($type){
            if($sub_type > 0){
                return response()->json(['status' => 0, 'message' => 'Cannot be deleted, have related records']);
            }else{
                $type->lang()->delete();
                $type->delete();
                $this->updateActivity();
                return response()->json(['status' => 1, 'message' => 'Topic deleted successfully']);
            }
            
        }else{
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }
    public function updateStatus(Request $req)
    {
        $att = Topic::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Topic::where('id', $req->id)
                        ->update([
                            'status' => 'active'
                ]);
            } else {
                Topic::where('id', $req->id)
                        ->update([
                            'status' => 'deactive'
                ]);
            }
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
