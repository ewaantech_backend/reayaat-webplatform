<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\ActivityLog;
use App\Models\Faq;
use App\Models\FaqLang;

class FAQController extends Controller
{
    use ActivityLog ;
    public function get(Request $request)
    {
        $search_field=$request->search_field ?? '';
        if($search_field)
        {
            $data=FaqLang::select('faq_id')->where('question', 'like', "%" . $search_field . "%")
            ->orwhere('answer','like', "%" . $search_field . "%")->get()->toArray();
            $da=[];
            foreach($data as  $d)
            {
                $da[]=$d['faq_id'];
            }
            $faq= Faq::with('lang')->whereIn('id',$da)->paginate(20);
        
        }else{
            $faq=Faq::with('lang')->paginate(20);
        }
        return view('admin.cms.faq.index',compact('faq','search_field'));
    }
    public function store(Request $request)
    {
        
        $faq= DB::transaction(function () use ($request) {
             $faq=Faq::create([
                 'status' => 'active',
             ]);
             $faq->lang()->createMany([
                [
                    'question' => $request->title_en,
                    'answer' => $request->content_en,
                    'language' => 'en',
                ],
                [
                    'question' => $request->title_ar,
                    'answer' => $request->content_ar,
                    'language' => 'ar',
                ],
             ]);
            
             $this->updateActivity();
             return $faq;
         });
         if($faq)
         {
             return [
                 "msg"=>"success"
             ];
         }else{
            return [
                "msg"=>"error"
            ];
         }
    }
    public function statusUpdate(Request $request)
    {
      
        $status=$request->status === 'Deactivate' ? 'deactive' : 'active' ;
        Faq::where('id',$request->id)
             ->update([
                'status'=>$status
        ]);
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
    public function edit($id)
    {
        $faq=Faq::with('lang')
                    ->where('id',$id)
                    ->first();
        return [
            'page'=>$faq,
            
        ];
    }

    public function update(Request $request)
    {
        $faq= DB::transaction(function () use ($request) {
           
            FaqLang::where('faq_id',$request->id)
            ->where('language','en')
            ->update(
               [
                   'question' => $request->title_en,
                   'answer' => $request->content_en,
                  
               ]);
               FaqLang::where('faq_id',$request->id)
               ->where('language','ar')
               ->update(
               [
                   'question' => $request->title_ar,
                   'answer' => $request->content_ar,
                   
               ]);
               
        });
        $this->updateActivity();
            return [
                'msg'=>"success"
            ];
    }

    public function destroy(Request $req)
    {
        $faq=Faq::find($req->id);
            $faq->lang()->delete();
            $faq->delete();
            $this->updateActivity();
         return response()->json(['status' => 1, 'message' => 'FAQ deleted successfully']);
    }

}
