<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Traits\ActivityLog;
use App\Models\Testimonial;
use App\Models\TestimonialLang;

class TestimonialController extends Controller
{
    use ActivityLog ;
    public function get(Request $request)
    {
        $search_field=$request->search_field ?? '';
        if($search_field)
        {
            $data=TestimonialLang::select('testimonial_id')->where('name', 'like', "%" . $search_field . "%")
            ->orwhere('company','like', "%" . $search_field . "%")
            ->orwhere('testimonial','like', "%" . $search_field . "%")->get()->toArray();
            $da=[];
            foreach($data as  $d)
            {
                $da[]=$d['testimonial_id'];
            }
            $testimonial= Testimonial::with('lang')->whereIn('id',$da)->paginate(20);
        
        }else{
            $testimonial=Testimonial::with('lang')->paginate(20);
        }
        return view('admin.testimonial.index',compact('testimonial','search_field'));
    }

    public function store(Request $request) {
        $testimonial_id = $request->id ;
        $rules = [
            'name_en' => 'required',
            'name_ar' => 'required',
            'company_en' => 'required',
            'company_ar' => 'required',
            'designation_en' => 'required',
            'designation_ar' => 'required',
            'testimonial_en' => 'required',
            'testimonial_ar' => 'required',
        ];
        $messages = [
            'name_en.required' => 'Title(EN) is required.',
            'name_ar.required' => 'Title(AR) is required.',
            'company_en.required' => 'Company(EN) is required.',
            'company_ar.required' => 'Company(AR) is required.',
            'designation_en.required' => 'Designation(EN) is required.',
            'designation_ar.required' => 'Designation(AR) is required.',
            'testimonial_en.required' => 'Testimonial(EN) is required.',
            'testimonial_ar.required' => 'Testimonial(AR) is required.',
        ];
        if($testimonial_id)
        {
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {

                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                $count = TestimonialLang::where('testimonial_id', $testimonial_id)->count();
                if($count > 0 ){
                    TestimonialLang::where('testimonial_id', $testimonial_id)->where('language', 'en')
                        ->update([
                            'name' => $request->name_en,
                            'designation' => $request->designation_en,
                            'company' => $request->company_en,
                            'testimonial' => $request->testimonial_en,
                    ]);
                    TestimonialLang::where('testimonial_id', $testimonial_id)->where('language', 'ar')
                            ->update([
                                'name' => $request->name_ar,
                                'designation' => $request->designation_ar,
                                'company' => $request->company_ar,
                                'testimonial' => $request->testimonial_ar,
                    ]);
                }else{
                    TestimonialLang::create([
                        'name' => $request->name_en,
                        'designation' => $request->designation_en,
                        'company' => $request->company_en,
                        'testimonial' => $request->testimonial_en,
                        'language' => 'en',
                        'testimonial_id' => $testimonial_id,
                    ]);
                    TestimonialLang::create([
                        'name' => $request->name_ar,
                        'designation' => $request->designation_ar,
                        'company' => $request->company_ar,
                        'testimonial' => $request->testimonial_ar,
                        'language' => 'ar',
                        'testimonial_id' => $testimonial_id,
                    ]);

                }

                $this->updateActivity();
                $msg = "Testimonial updated successfully";
                return response()->json(['status' => 1, 'message' => $msg]);
               
            }
            
        }else{
            $validator = Validator::make($request->all(), $rules, $messages);
            if (!$validator->passes()) {
                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            } else {

                $testimonial = Testimonial::create([
                    'status' => "deactive",
                ]);

                $testimonial->lang()->createMany([
                    [
                        'name' => $request->name_en,
                        'designation' => $request->designation_en,
                        'company' => $request->company_en,
                        'testimonial' => $request->testimonial_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->name_ar,
                        'designation' => $request->designation_ar,
                        'company' => $request->company_ar,
                        'testimonial' => $request->testimonial_ar,
                        'language' => 'ar',
                    ],
                 ]);
               
                if($testimonial)
                {
                    $this->updateActivity();
                    return response()->json(['status' => 1, 'message' => 'Testimonial created successfully']);
                }else{
                    return response()->json(['status' => 2, 'message' => 'Sorry something went wrong.']);
                }
                
            }
        }
    }
    public function edit($id)
    {
        $testimonial=Testimonial::with('lang')
                    ->where('id',$id)
                    ->first();
        return [
            'page'=>$testimonial,
            
        ];
    }
    public function statusUpdate(Request $request)
    {
      
        $status=$request->status === 'Deactivate' ? 'deactive' : 'active' ;
        Testimonial::where('id',$request->id)
             ->update([
                'status'=>$status
        ]);
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
    public function destroy(Request $req)
    {
        $testimonial=Testimonial::find($req->id);
            $testimonial->lang()->delete();
            $testimonial->delete();
            $this->updateActivity();
         return response()->json(['status' => 1, 'message' => 'Testimonial deleted successfully']);
    }
    public function upload_image(Request $request) {
        $rules = [
            'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'photo.max' => 'The image must be less than 2Mb in size',
            'photo.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = request()->file('photo');
            $path = $file->store("testimonial", ['disk' => 'public_uploads']);
            $full_path = url('uploads/' . $path);

            if($request->id_pg){
                Testimonial::where('id', $request->id_pg)
                        ->update([
                        "{$request->type}" => $full_path
                ]);
            }else{
                $testimonial_created = Testimonial::create([
                        "{$request->type}" => $full_path
                ]);
            }
           
            $id_pg = $request->id_pg ? $request->id_pg : $testimonial_created->id;
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'File uploaded successfully', 'path' => $full_path, 'id_pg' => $id_pg, 'type' => $request->type]);
        }
    }

    public function detete_img(Request $request) {
        $testimonial = Testimonial::where('id', $request->id)->first();
        $type = $request->type;
        Testimonial::where('id', $request->id)
                ->update([
                    $type => NULL
        ]);
        if (!empty($testimonial)) {
            $file_path = $testimonial[$type];
            if(is_file($file_path))
            unlink($file_path);
        }
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => 'File deleted successfully', 'id' => $request->id, 'type' => $request->type]);
    }
}
