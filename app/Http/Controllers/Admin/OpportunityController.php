<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\ActivityLog;
use App\Models\Opportunity;
use App\Models\OpportunityLang;

class OpportunityController extends Controller
{
    use ActivityLog ;
    public function get()
    {
        $opportunity=Opportunity::with('lang') ->paginate(20);
        return view('admin.settings.opportunity.index',compact('opportunity'));
    }

    public function edit($id)
    {
        $Opportunity_data=Opportunity::with('lang')
                ->where('id',$id)->first();       
        return[
            'opportunity'=>$Opportunity_data
        ];
    }
    public function update(Request $request)
    {
       $sement= DB::transaction(function () use ($request ) {
       
           OpportunityLang::where('language','en')
            ->where('opportunity_per_year_id',$request->id)
            ->update([
                'title'=>$request->title_en,
                'content'=>$request->content_en
            ]);
            OpportunityLang::where('language','ar')
            ->where('opportunity_per_year_id',$request->id)
            ->update([
                'title'=>$request->title_ar,
                'content'=>$request->content_ar
            ]);
            
        });
        $this->updateActivity();
        return [
            "msg"=>"success"

        ];

    }

    public function statusUpdate(Request $request)
    {
      
        $status=$request->status === 'Deactivate' ? 'deactive' : 'active' ;
        Opportunity::where('id',$request->id)
             ->update([
                'status'=>$status
        ]);
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
}
