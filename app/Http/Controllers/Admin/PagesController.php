<?php

namespace App\Http\Controllers\Admin;

use Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\ActivityLog;
use App\Models\Page;
use App\Models\AboutUsPageInfo;
use App\Models\AboutUsSeo;
use App\Models\TermsConditionPageInfo;
use App\Models\TermsConditionSeo;
use App\Models\PrivacyPolicyPageInfo;
use App\Models\PrivacyPolicySeo;
use App\Models\ContactUsPageInfo;
use App\Models\ContactUsSeo;
use App\Models\LookingSponsorSeo;
use App\Models\LookingSponsorShowcasePm;
use App\Models\LookingSponsorWhatInIt;

class PagesController extends Controller
{
    use ActivityLog ;
    public function get()
    {
        $page=Page::with('aboutUsPageInfo','termsPageInfo','privacyPageInfo','contactPageInfo','sponsorShowcasePm')->where('id','!=',6)->get();
        $cms_pages = (Config::get('constants.cms_pages'));
        return view('admin.cms.pages.index',compact('page','cms_pages'));
    }

    public function create(Request $req)
    {
        $id = $req->id;
        
        if($id == 1){
            $page=Page::with('aboutUsPageInfo')->where('id',$id)->first();
            return view('admin.cms.pages.about_create',compact('page','id'));
        }elseif($id == 2){
            $page=Page::with('termsPageInfo')->where('id',$id)->first();
            return view('admin.cms.pages.terms_create',compact('page','id'));
        }elseif($id == 3){
            $page=Page::with('privacyPageInfo')->where('id',$id)->first();
            return view('admin.cms.pages.privacy_create',compact('page','id'));
        }elseif($id == 4){
            $page=Page::with('contactPageInfo')->where('id',$id)->first();
            return view('admin.cms.pages.contact_create',compact('page','id'));
        }elseif($id == 5){
            $page=Page::with('sponsorShowcasePm')->where('id',$id)->first();
            return view('admin.cms.pages.sponsor_create',compact('page','id'));
        }
        
    }
    public function about_tabs(Request $req)
    {
        $id = $req->id;
        if ($req->activeTab == 'PAGE INFO') {
            $page=Page::with('aboutUsPageInfo')->first();
            return view('admin.cms.pages.about_pageinfo',compact('page','id'));
        }elseif($req->activeTab == 'SEO'){
            $page=Page::with('aboutUsSeo')->first();
            return view('admin.cms.pages.about_seo',compact('page','id'));
        }
    }
    public function about_pageinfo(Request $req)
    {
        $id = $req->id;
        $page = array();
        AboutUsPageInfo::where('language', 'en')
            ->where('pages_id', $req->id)
            ->update([
                'title' => $req->title_en,
                'content' => $req->content_en,
                'sub_title' => $req->subtitle_en,
                'description' => $req->desc_en,
                'sub_content' => $req->subcont_en,
            ]);
            AboutUsPageInfo::where('language', 'ar')
            ->where('pages_id', $req->id)
            ->update([
                'title' => $req->title_ar,
                'content' => $req->content_ar,
                'sub_title' => $req->subtitle_ar,
                'description' => $req->desc_ar,
                'sub_content' => $req->subcont_ar,
            ]);


        $msg = 'Page info updated successfully';
        
        $page = Page::with('aboutUsSeo')->first();
        $html = view('admin.cms.pages.about_seo')->with(compact('id', 'page'))->render();
        return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
    }

    public function about_seo(Request $req)
    {
        $id = $req->id;
        AboutUsSeo::where('language', 'en')
            ->where('pages_id', $req->id)
            ->update([
                'meta_title' => $req->seo_title_en,
                'description' => $req->seo_content_en,
            ]);
            AboutUsSeo::where('language', 'ar')
            ->where('pages_id', $req->id)
            ->update([
                'meta_title' => $req->seo_title_ar,
                'description' => $req->seo_content_ar,
            ]);


        $msg = 'Seo updated successfully';
        return response()->json(['status' => 1, 'message' => $msg]);
    }

    public function upload_image(Request $request) {
        $rules = [
            'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'photo.max' => 'The image must be less than 2Mb in size',
            'photo.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = request()->file('photo');
            $path = $file->store("about_us", ['disk' => 'public_uploads']);
            if($request->id_pg){
                AboutUsPageInfo::where('language', 'en')
                ->where('pages_id', $request->id_pg)
                        ->update([
                        "{$request->type}" => $path
                ]);
            }
           
            $id_pg = $request->id_pg ? $request->id_pg : '';
            $full_path = url('uploads/' . $path);
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'File uploaded successfully', 'path' => $full_path, 'id_pg' => $id_pg, 'type' => $request->type]);
        }
    }

    public function detete_img(Request $request) {
        $aboutus = AboutUsPageInfo::where('pages_id', $request->id)->first();
        $type = $request->type;
        AboutUsPageInfo::where('pages_id', $request->id)
                ->update([
                    $type => ''
        ]);
        if (!empty($aboutus)) {
            $file_path = public_path() . '/uploads/' . $aboutus[$type];
            if(is_file($file_path))
            unlink($file_path);
        }
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => 'File deleted successfully', 'id' => $request->id, 'type' => $request->type]);
    }

    public function terms_tabs(Request $req)
    {
        $id = $req->id;
        if ($req->activeTab == 'PAGE INFO') {
            $page=Page::with('termsPageInfo')->where('id',$id)->first();
            return view('admin.cms.pages.terms_pageinfo',compact('page','id'));
        }elseif($req->activeTab == 'SEO'){
            $page=Page::with('termsSeo')->where('id',$id)->first();
            return view('admin.cms.pages.terms_seo',compact('page','id'));
        }
    }

    public function terms_pageinfo(Request $req)
    {
        $id = $req->id;
        $page = array();
        TermsConditionPageInfo::where('language', 'en')
        ->where('pages_id', $req->id)
        ->update([
            'title' => $req->title_en,
            'content' => $req->desc_en,
        ]);
        TermsConditionPageInfo::where('language', 'ar')
        ->where('pages_id', $req->id)
        ->update([
            'title' => $req->title_ar,
            'content' => $req->desc_ar,
        ]);

        $msg = 'Page info updated successfully';
        
        $page = Page::with('termsSeo')->where('id',$id)->first();
        $html = view('admin.cms.pages.terms_seo')->with(compact('id', 'page'))->render();
        return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
    }

    public function terms_seo(Request $req)
    {
        $id = $req->id;
        TermsConditionSeo::where('language', 'en')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_en,
            'description' => $req->seo_content_en,
        ]);
        TermsConditionSeo::where('language', 'ar')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_ar,
            'description' => $req->seo_content_ar,
        ]);


        $msg = 'Seo updated successfully';
        return response()->json(['status' => 1, 'message' => $msg]);
    }

    public function privacy_tabs(Request $req)
    {
        $id = $req->id;
        if ($req->activeTab == 'PAGE INFO') {
            $page=Page::with('privacyPageInfo')->where('id',$id)->first();
            return view('admin.cms.pages.privacy_pageinfo',compact('page','id'));
        }elseif($req->activeTab == 'SEO'){
            $page=Page::with('privacySeo')->where('id',$id)->first();
            return view('admin.cms.pages.privacy_seo',compact('page','id'));
        }
    }

    public function privacy_pageinfo(Request $req)
    {
        $id = $req->id;
        $page = array();
        PrivacyPolicyPageInfo::where('language', 'en')
        ->where('pages_id', $req->id)
        ->update([
            'title' => $req->title_en,
            'content' => $req->desc_en,
        ]);
        PrivacyPolicyPageInfo::where('language', 'ar')
        ->where('pages_id', $req->id)
        ->update([
            'title' => $req->title_ar,
            'content' => $req->desc_ar,
        ]);

        $msg = 'Page info updated successfully';
        
        $page = Page::with('privacySeo')->where('id',$id)->first();
        $html = view('admin.cms.pages.privacy_seo')->with(compact('id', 'page'))->render();
        return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
    }

    public function privacy_seo(Request $req)
    {
        $id = $req->id;
        PrivacyPolicySeo::where('language', 'en')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_en,
            'description' => $req->seo_content_en,
        ]);
        PrivacyPolicySeo::where('language', 'ar')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_ar,
            'description' => $req->seo_content_ar,
        ]);


        $msg = 'Seo updated successfully';
        return response()->json(['status' => 1, 'message' => $msg]);
    }

    public function contact_tabs(Request $req)
    {
        $id = $req->id;
        if ($req->activeTab == 'PAGE INFO') {
            $page=Page::with('contactPageInfo')->where('id',$id)->first();
            return view('admin.cms.pages.contact_pageinfo',compact('page','id'));
        }elseif($req->activeTab == 'SEO'){
            $page=Page::with('contactSeo')->where('id',$id)->first();
            return view('admin.cms.pages.contact_seo',compact('page','id'));
        }
    }

    public function contact_pageinfo(Request $req)
    {
        $id = $req->id;
        $page = array();
        ContactUsPageInfo::where('language', 'en')
        ->where('pages_id', $req->id)
        ->update([
            'title' => $req->title_en,
            'content' => $req->desc_en,
            'address' => $req->address_en,
            'email' => $req->email,
            'phone' => $req->phone,
        ]);
        ContactUsPageInfo::where('language', 'ar')
        ->where('pages_id', $req->id)
        ->update([
            'title' => $req->title_ar,
            'content' => $req->desc_ar,
            'address' => $req->address_ar,
        ]);

        $msg = 'Page info updated successfully';
        
        $page = Page::with('contactSeo')->where('id',$id)->first();
        $html = view('admin.cms.pages.contact_seo')->with(compact('id', 'page'))->render();
        return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
    }

    public function contact_seo(Request $req)
    {
        $id = $req->id;
        ContactUsSeo::where('language', 'en')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_en,
            'description' => $req->seo_content_en,
        ]);
        ContactUsSeo::where('language', 'ar')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_ar,
            'description' => $req->seo_content_ar,
        ]);


        $msg = 'Seo updated successfully';
        return response()->json(['status' => 1, 'message' => $msg]);
    }

    public function sponsor_tabs(Request $req)
    {
        $id = $req->id;
        if ($req->activeTab == 'SHOWCASE PROGRAM') {
            $page=Page::with('sponsorShowcasePm')->where('id',$id)->first();
            return view('admin.cms.pages.sponsor_showcasepm',compact('page','id'));
        }elseif($req->activeTab == 'SEO'){
            $page=Page::with('sponsorSeo')->where('id',$id)->first();
            return view('admin.cms.pages.sponsor_seo',compact('page','id'));
        }else{
            $page=Page::with('sponsorWhatInIt')->where('id',$id)->first();
            return view('admin.cms.pages.sponsor_whatinit',compact('page','id'));
        }
    }

    public function sponsor_showcase_program(Request $req)
    {
        $id = $req->id_pg;
        $page = array();
        LookingSponsorShowcasePm::where('language', 'en')
        ->where('pages_id', $id)
        ->update([
            'title' => $req->title_en,
            'step1_title' => $req->step1_title_en,
            'step1_desc' => $req->step1_desc_en,
            'step2_title' => $req->step2_title_en,
            'step2_desc' => $req->step2_desc_en,
            'step3_title' => $req->step3_title_en,
            'step3_desc' => $req->step3_desc_en,
        ]);
        LookingSponsorShowcasePm::where('language', 'ar')
        ->where('pages_id', $id)
        ->update([
            'title' => $req->title_ar,
            'step1_title' => $req->step1_title_ar,
            'step1_desc' => $req->step1_desc_ar,
            'step2_title' => $req->step2_title_ar,
            'step2_desc' => $req->step2_desc_ar,
            'step3_title' => $req->step3_title_ar,
            'step3_desc' => $req->step3_desc_ar,
        ]);

        $msg = 'Showcase program updated successfully';
        
        $page = Page::with('sponsorWhatInIt')->where('id',$id)->first();
        $html = view('admin.cms.pages.sponsor_whatinit')->with(compact('id', 'page'))->render();
        return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
    }

    public function sponsor_whatinit(Request $req)
    {
        $id = $req->id_pg;
        $page = array();
        LookingSponsorWhatInIt::where('language', 'en')
        ->where('pages_id', $id)
        ->update([
            'title' => $req->title_en,
            'content' => $req->content_en,
            'sub_title1' => $req->step1_title_en,
            'sub_content1' => $req->step1_desc_en,
            'sub_title2' => $req->step2_title_en,
            'sub_content2' => $req->step2_desc_en,
            'sub_title3' => $req->step3_title_en,
            'sub_content3' => $req->step3_desc_en,
            'sub_title4' => $req->step4_title_en,
            'sub_content4' => $req->step4_desc_en,
        ]);
        LookingSponsorWhatInIt::where('language', 'ar')
        ->where('pages_id', $id)
        ->update([
            'title' => $req->title_ar,
            'content' => $req->content_ar,
            'sub_title1' => $req->step1_title_ar,
            'sub_content1' => $req->step1_desc_ar,
            'sub_title2' => $req->step2_title_ar,
            'sub_content2' => $req->step2_desc_ar,
            'sub_title3' => $req->step3_title_ar,
            'sub_content3' => $req->step3_desc_ar,
            'sub_title4' => $req->step4_title_ar,
            'sub_content4' => $req->step4_desc_ar,
        ]);

        $msg = 'Whats in it updated successfully';
        
        $page = Page::with('sponsorSeo')->where('id',$id)->first();
        $html = view('admin.cms.pages.sponsor_seo')->with(compact('id', 'page'))->render();
        return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
    }

    public function sponsor_seo(Request $req)
    {
        $id = $req->id;
        LookingSponsorSeo::where('language', 'en')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_en,
            'description' => $req->seo_content_en,
        ]);
        LookingSponsorSeo::where('language', 'ar')
        ->where('pages_id', $req->id)
        ->update([
            'meta_title' => $req->seo_title_ar,
            'description' => $req->seo_content_ar,
        ]);


        $msg = 'Seo updated successfully';
        return response()->json(['status' => 1, 'message' => $msg]);
    }

    public function sponsor_upload_image(Request $request) {
        $rules = [
            'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'photo.max' => 'The image must be less than 2Mb in size',
            'photo.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = request()->file('photo');
            $path = $file->store("looking_for_sponsor", ['disk' => 'public_uploads']);
            if($request->id_pg){
                LookingSponsorWhatInIt::where('language', 'en')
                ->where('pages_id', $request->id_pg)
                        ->update([
                        "{$request->type}" => $path
                ]);
            }
           
            $id_pg = $request->id_pg ? $request->id_pg : '';
            $full_path = url('uploads/' . $path);
            $this->updateActivity();
            return response()->json(['status' => 1, 'message' => 'File uploaded successfully', 'path' => $full_path, 'id_pg' => $id_pg, 'type' => $request->type]);
        }
    }

    public function sponsor_detete_img(Request $request) {
        $aboutus = AboutUsPageInfo::where('pages_id', $request->id)->first();
        $type = $request->type;
        LookingSponsorWhatInIt::where('pages_id', $request->id)
                ->update([
                    $type => ''
        ]);
        if (!empty($aboutus)) {
            $file_path = public_path() . '/uploads/' . $aboutus[$type];
            if(is_file($file_path))
            unlink($file_path);
        }
        $this->updateActivity();
        return response()->json(['status' => 1, 'message' => 'File deleted successfully', 'id' => $request->id, 'type' => $request->type]);
    }
    
}
