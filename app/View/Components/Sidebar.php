<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Sidebar extends Component
{
    public $menuArray;
    public $adminLogoArray;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->adminLogoArray = array(
            'uri' => route('dashboard.get'),
            'img' => 'assets/images/logo.png'
        );
 
        $this->menuArray = [
            'dashboard' => [
                'position' => 1,
                'level' => 1,
                'name' => 'Dashboard',
                'icon' => 'ti-dashboard',
                'uri' => route('dashboard.get'),
            ],
            'cms' => [
                'position' => 2,
                'level' => 2,
                'name' => 'CMS',
                'icon' => 'ti-book',
                'submenus' => [
                    [
                        'name' => 'Slides',
                        'icon' => '',
                        'uri' => route('admin.slide.get'),
                    ],
                    [
                        'name' => 'FAQs',
                        'icon' => '',
                        'uri' => route('admin.faq.get'),
                    ],
                    [
                        'name' => 'Pages',
                        'icon' => '',
                        'uri' => route('admin.pages.get'),
                    ],
                ]
            ],
            'packages' => [
                'position' => 3,
                'level' => 2,
                'name' => 'Packages',
                'icon' => 'ti-gallery',
                'submenus' => [
                    [
                        'name' => 'Features',
                        'icon' => '',
                        'uri' => route('admin.feature.get'),
                    ],
                ]
            ],
            'testimonial' => [
                'position' => 4,
                'level' => 1,
                'name' => 'Testimonial',
                'icon' => 'ti-user',
                'uri' => route('admin.testimonial.get'),
            ],
            'logs' => [
                'position' => 5,
                'level' => 2,
                'name' => 'Logs',
                'icon' => 'ti-timer',
                'submenus' => [
                    [
                        'name' => 'Login activity',
                        'icon' => '',
                        'uri' => route('logs'),
                    ],
                ],
            ],
            'setting' => [
                'position' => 6,
                'level' => 2,
                'name' => 'Settings',
                'icon' => 'ti-settings',
                'submenus' => [
                    [
                        'name' => 'Admin users',
                        'icon' => '',
                        'uri' => route('admin.user.get')
                    ],
                    [
                        'name' => 'Category',
                        'icon' => '',
                        'uri' => route('admin.category.get')
                    ],
                    [
                        'name' => 'Type',
                        'icon' => '',
                        'uri' => route('admin.type.get')
                    ],
                    [
                        'name' => 'Topic',
                        'icon' => '',
                        'uri' => route('admin.topic.get')
                    ],
                    [
                        'name' => 'Opportunity',
                        'icon' => '',
                        'uri' => route('admin.opportunity.get')
                    ],
                    [
                        'name' => 'Other settings',
                        'icon' => '',
                        'uri' => route('admin.other.get')
                    ],
                ],
            ],
        ];
        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.sidebar');
    }
}
