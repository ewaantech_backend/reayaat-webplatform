<?php

namespace App\Listeners;

// use Illuminate\Contracts\Queue\ShouldQueue;
// use Illuminate\Queue\InteractsWithQueue;
use App\Events\SendToken;
use App\Mail\TokenGeneratingEmail;
use Illuminate\Support\Facades\Mail;

class GenerateToken
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SendToken $event)
    {
       
        Mail::to($event->email)->send(new TokenGeneratingEmail(
            $event->token)
        );
    }
}
