<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="pages";

    public function aboutUsPageInfo()
    {
        return $this->hasMany(AboutUsPageInfo::class, 'pages_id');
    }
    public function aboutUsSeo()
    {
        return $this->hasMany(AboutUsSeo::class, 'pages_id');
    }
    public function termsPageInfo()
    {
        return $this->hasMany(TermsConditionPageInfo::class, 'pages_id');
    }
    public function termsSeo()
    {
        return $this->hasMany(TermsConditionSeo::class, 'pages_id');
    }
    public function privacyPageInfo()
    {
        return $this->hasMany(PrivacyPolicyPageInfo::class, 'pages_id');
    }
    public function privacySeo()
    {
        return $this->hasMany(PrivacyPolicySeo::class, 'pages_id');
    }
    public function contactPageInfo()
    {
        return $this->hasMany(ContactUsPageInfo::class, 'pages_id');
    }
    public function contactSeo()
    {
        return $this->hasMany(ContactUsSeo::class, 'pages_id');
    }
    public function sponsorShowcasePm()
    {
        return $this->hasMany(LookingSponsorShowcasePm::class, 'pages_id');
    }
    public function sponsorWhatInIt()
    {
        return $this->hasMany(LookingSponsorWhatInIt::class, 'pages_id');
    }
    public function sponsorSeo()
    {
        return $this->hasMany(LookingSponsorSeo::class, 'pages_id');
    }
    public function hometPageInfo()
    {
        return $this->hasMany(HomePageInfo::class, 'pages_id');
    }
    public function homeSeo()
    {
        return $this->hasMany(HomeSeo::class, 'pages_id');
    }
    
}
