<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="feature";
    
    public function lang()
    {
        return $this->hasMany(FeatureLang::class, 'feature_id');
    }
}
