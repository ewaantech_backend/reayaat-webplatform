<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class OpportunityLang extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
        protected $guarded = [];
        /**
         * $table variable
         *
         * @var string
         */
    
        protected $table="opportunity_per_year_i18n";
}
