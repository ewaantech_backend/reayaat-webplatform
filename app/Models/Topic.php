<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="topic";
    
    public function lang()
    {
        return $this->hasMany(TopicLang::class, 'topic_id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\Topic', 'parent_id')->with('lang')->where('deleted_at', NULL);
    }
    public function getParentsNames()
    {
        if ($this->parent) {
        return $this->parent->getParentsNames() . $this->parent->lang[0]->name;
        } else {
        return $this->name;
        }
    }
}
