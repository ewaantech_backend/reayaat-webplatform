<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="type";
    
    public function lang()
    {
        return $this->hasMany(TypeLang::class, 'type_id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\Type', 'parent_id')->with('lang')->where('deleted_at', NULL);
    }
    public function getParentsNames()
    {
        if ($this->parent) {
        return $this->parent->getParentsNames() . $this->parent->lang[0]->name;
        } else {
        return $this->name;
        }
    }
}
