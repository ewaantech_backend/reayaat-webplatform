<?php

namespace App\Traits;

use App\Models\User;
use Carbon\Carbon;

trait ActivityLog
{
    public function updateActivity()
    {
        User::where('id',auth()->user()->id)->update([
            'last_activity_date'=>Carbon::now()
        ]);

    }
    
}