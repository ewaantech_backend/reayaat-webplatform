<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordGenerateEmail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $token;

    public function __construct($password, $name, $email, $subject, $mail_from, $url) {
        $this->password = $password;
        $this->email = $email;
        $this->name = $name;
        $this->subject = $subject;
        $this->mail_from = $mail_from;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject($this->subject)
        ->markdown('mail.credentials')
        ->with('name', $this->name)
        ->with('email', $this->email)
        ->with('password', $this->password)
        ->with('mail_from', $this->mail_from)
        ->with('url', $this->url);            
    }

}
