<?php

namespace App\Services;
use App\Mail\PasswordGenerateEmail;
use Illuminate\Support\Facades\Mail;

class SendEmail {

    public static function generateEmail($to, $otp = '', $name = '', $subject = '', $from = '', $url = '') {
        if ($from == 'user') {
            $emai_send = Mail::to($to)->send(new PasswordGenerateEmail($otp, $name, $to, $subject, $from, $url));
            return $emai_send;
        }
    }
    
}
