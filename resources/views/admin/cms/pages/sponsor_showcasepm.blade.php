<form id="frm_create_sponsor_pageinfo" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="row">
                <input type="hidden" id="id_pg" name="id_pg" value="{{$id}}">
                @csrf
                <div class="col-md-6">
                    <label>Title (EN)</label>
                    <textarea class="form-control area" name="title_en" id="title_en" placeholder="Title (EN)">{{$page->sponsorShowcasePm[0]->title}}</textarea>
                </div>
                <div class="col-md-6">
                     <label>Title (AR)</label>
                    <textarea class="form-control area" name="title_ar" id="title_ar"
                        style="text-align:right !important" placeholder="Title (AR)" >{{$page->sponsorShowcasePm[1]->title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle1 (EN)</label>
                    <textarea class="form-control area" name="step1_title_en" id="step1_title_en" placeholder="Subtitle1 (EN)">{{$page->sponsorShowcasePm[0]->step1_title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle1 (AR)</label>
                    <textarea class="form-control area" name="step1_title_ar" id="step1_title_ar"
                        style="text-align:right !important" placeholder="Subtitle1 (AR)" >{{$page->sponsorShowcasePm[1]->step1_title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent1 (EN)</label>
                    <textarea class="form-control area1" name="step1_desc_en" id="step1_desc_en" placeholder="Subcontent1 (EN)">{{$page->sponsorShowcasePm[0]->step1_desc}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent1 (AR)</label>
                    <textarea class="form-control area1" name="step1_desc_ar" id="step1_desc_ar"
                        style="text-align:right !important" placeholder="Subcontent1 (AR)" >{{$page->sponsorShowcasePm[1]->step1_desc}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle2 (EN)</label>
                    <textarea class="form-control area" name="step2_title_en" id="step2_title_en" placeholder="Subtitle2 (EN)">{{$page->sponsorShowcasePm[0]->step2_title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle2 (AR)</label>
                    <textarea class="form-control area" name="step2_title_ar" id="step2_title_ar"
                        style="text-align:right !important" placeholder="Subtitle2 (AR)" >{{$page->sponsorShowcasePm[1]->step2_title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent2 (EN)</label>
                    <textarea class="form-control area1" name="step2_desc_en" id="step2_desc_en" placeholder="Subcontent2 (EN)">{{$page->sponsorShowcasePm[0]->step2_desc}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent2 (AR)</label>
                    <textarea class="form-control area1" name="step2_desc_ar" id="step2_desc_ar"
                        style="text-align:right !important" placeholder="Subcontent2 (AR)" >{{$page->sponsorShowcasePm[1]->step2_desc}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle3 (EN)</label>
                    <textarea class="form-control area" name="step3_title_en" id="step3_title_en" placeholder="Subtitle3 (EN)">{{$page->sponsorShowcasePm[0]->step3_title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle3 (AR)</label>
                    <textarea class="form-control area" name="step3_title_ar" id="step3_title_ar"
                        style="text-align:right !important" placeholder="Subtitle3 (AR)" >{{$page->sponsorShowcasePm[1]->step3_title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent3 (EN)</label>
                    <textarea class="form-control area1" name="step3_desc_en" id="step3_desc_en" placeholder="Subcontent3 (EN)">{{$page->sponsorShowcasePm[0]->step3_desc}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent3 (AR)</label>
                    <textarea class="form-control area1" name="step3_desc_ar" id="step3_desc_ar"
                        style="text-align:right !important" placeholder="Subcontent3 (AR)" >{{$page->sponsorShowcasePm[1]->step3_desc}}</textarea>
                </div>
               
                
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="">Back To Listing</a>
        </div>
    </div>
</form>
<style>

</style>
