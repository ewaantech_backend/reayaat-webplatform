<form id="frm_create_contact_pageinfo" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="row">
                <input type="hidden" id="id_pg" name="id_pg" value="{{$id}}">
                @csrf
                <div class="col-md-6">
                    <label>Title (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="title_en" id="title_en" placeholder="">{{$page->contactPageInfo[0]->title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Title (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="title_ar" id="title_ar"
                        style="text-align:right !important" placeholder="" >{{$page->contactPageInfo[1]->title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Short Description (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control" id="summaryen" name="summaryen" placeholder="">{{$page->contactPageInfo[0]->content}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Short Description (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control" id="summaryar" name="summaryar" placeholder="">{{$page->contactPageInfo[1]->content}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Address (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="address_en" id="address_en" placeholder="">{{$page->contactPageInfo[0]->address}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Address (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="address_ar" id="address_ar"
                        style="text-align:right !important" placeholder="" >{{$page->contactPageInfo[1]->address}}</textarea>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Email <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="" type="email" id="email" name="email" value="{{$page->contactPageInfo[0]->email}}"/>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Phone <span class="text-danger">*</span></label>
                    <input class="form-control form-white" type="text" name="country_code" value="+966" id="country_code" readonly/>
                </div>
                <div class="col-md-4">
                    <label>&nbsp;</label>
                    <input class="form-control form-white" placeholder="" type="text" name="phone" id="phone" value="{{$page->contactPageInfo[0]->phone}}"/>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="">Back To Listing</a>
        </div>
    </div>
</form>
<style>

</style>
