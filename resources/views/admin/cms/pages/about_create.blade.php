<div class="modal-header">
    <h4 class="modal-title"><strong>
            Edit About Us
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="about_tab">
        <li class="nav-item w-25 "> <a class="nav-link active" href="#page_info" data-id="{{ isset($id) ? $id  : '' }}" role="tab"><span class="hidden-xs-down">PAGE INFO</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#seo" data-id="{{ isset($id) ? $id  : '' }}" role="tab"><span class="hidden-xs-down">SEO</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('admin.cms.pages.about_pageinfo')
</div>


<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #formModal { overflow-y:auto; }
 
</style>
<script>
    CKEDITOR.replace( 'summaryen', {
        startupFocus: true,
        filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
    CKEDITOR.replace('summaryar', {
    startupFocus: true,
    contentsLangDirection: 'rtl',
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
    } );


</script>

<script>
   $('#page-popup').modal({backdrop: 'static', keyboard: false});

    $(document).on('click', '.save-btn', function () {
        $("#submit_action").val('save');
    });
    $(document).on('click', '.save-and-continue', function () {
        $("#submit_action").val('continue');
    });

    $(document).on('click', '.tab_back', function () {
        
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('about_tabs')}}",
            data: {'activeTab': 'PAGE INFO', id: id},
            success: function (result) {
                $('#about_tab a[href="#page_info"]').tab('show');
                $('.tab-content').html(result);

                CKEDITOR.replace( 'summaryen', {
                    startupFocus: true,
                    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
                CKEDITOR.replace('summaryar', {
                startupFocus: true,
                contentsLangDirection: 'rtl',
                filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form'
                } );

            }
        });
    });

    $(document).on('click', '.save-btn-seo', function () {

        $("#frm_create_about_seo").validate({
        ignore: [],
        rules: {
            seo_title_en  : {
                required: true,
            },
            seo_title_ar: {
                required: true,
            },
            seo_content_en  : {
                required: true,
            },
            seo_content_ar: {
                required: true,
            },
        
        },
        
        messages: {
            seo_title_en: {
            required:"Meta Title(EN) required",
            },
            seo_title_ar: {
            required: "Meta Title (AR) required",
            },
            seo_content_en  : {
                required: "Description(EN) required",
            },
            seo_content_ar: {
                required: "Description(AR) required",
            },
        },
            
        
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form) {
            let edit_val=$('#id_pg').val();
            
            if(edit_val){
                $.ajax({
                    type:"POST",
                    url: "{{route('pages.aboutus.seo')}}",
                    data:{
                        seo_title_en:$('#seo_title_en').val(),
                        seo_title_ar:$('#seo_title_ar').val(),
                        seo_content_en:$('#seo_content_en').val(),
                        seo_content_ar:$('#seo_content_ar').val(),
                        id:edit_val

                    },
                    success: function(data){
                        if (data.status == 1) {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            window.location.href = '{{route("admin.pages.get")}}';
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                            $("#about_tab").tabs({cache: true});
                        }
                        
                        },
                        error: function (err) {
                            
                        }
                });
            }

        }
    });

    });

    $(document).on('click', '.save-and-continue,.save-btn', function () {
    $("#frm_create_about_pageinfo").validate({
        ignore: [],
        rules: {
        title_en  : {
            required: true,
        },
        title_ar: {
            required: true,
        },
        content_en  : {
            required: true,
        },
        content_ar: {
            required: true,
        },
        subtitle_en  : {
            required: true,
        },
        subtitle_ar: {
            required: true,
        },
        summaryen :{
            required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement();
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                return editorcontent.length === 0;
                    }

            },
        summaryar :{
                required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement();
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                    return editorcontent.length === 0;
                },
            },
        subcont_en  : {
            required: true,
        },
        subcont_ar: {
            required: true,
        },
        },
        
        messages: {
            title_en: {
                    required:"Title(EN) required",

                    },
            title_ar: {
                    required: "Title (AR) required",
                    },
            content_en  : {
                required: "Content(EN) required",
            },
            content_ar: {
                required: "Content(AR) required",
            },
            subtitle_en  : {
                required: "Subtitle(EN) required",
            },
            subtitle_ar: {
                required: "Subtitle(EN) required",
            },
            summaryen: {
                required: "Description (EN) required",
            },
            summaryar: {
                required: "Description(AR) required",
            },
            subcont_en  : {
                required: "Subcontent (EN) required",
            },
            subcont_ar: {
                required: "Subcontent (AR) required",
            }
        },
            
        
            errorPlacement: function (error, element) {
            if (element.attr("type") == "checkbox") {
                error.insertBefore(element);
            } else {
                error.insertAfter(element);
            }
        },
            submitHandler: function(form) {
            let edit_val=$('#id_pg').val();
            
            if(edit_val){
                $.ajax({
                    type:"POST",
                    url: "{{route('pages.aboutus.pageinfo')}}",
                    data:{
                        title_en:$('#title_en').val(),
                        title_ar:$('#title_ar').val(),
                        content_en:$('#content_en').val(),
                        content_ar:$('#content_ar').val(),
                        subtitle_en:$('#subtitle_en').val(),
                        subtitle_ar:$('#subtitle_ar').val(),
                        desc_en: CKEDITOR.instances['summaryen'].getData(),
                        desc_ar:CKEDITOR.instances['summaryar'].getData(),
                        subcont_en:$('#subtitle_en').val(),
                        subcont_ar:$('#subtitle_ar').val(),
                        id:edit_val

                    },
                    success: function(data){
                        if (data.status == 1) {
                            if ($("#submit_action").val() == 'continue') {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                $('#about_tab a[href="#seo"]').tab('show');
                                $('.tab-content').html(data.result);
                            } else {
                                
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            }
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                            $("#about_tab").tabs({cache: true});
                        }
                        
                        },
                        error: function (err) {
                            
                        }
                });
            }

        }
    });
});

$("button[data-dismiss=modal]").click(function () {
    location.reload();
});
$('.nav-tabs a').on('click', function (e) {
    var id = $(this).data("id");
    var x = $(e.target).text();
    var location = $(this).attr('href');
    if (id != '') {
        $.ajax({
            type: "GET",
            url: "{{route('about_tabs')}}",
            data: {'activeTab': x, id: id},
            success: function (result) {
                $('#about_tab a[href="'+location+'"]').tab('show');
                $('.tab-content').html(result);

                CKEDITOR.replace( 'summaryen', {
                    startupFocus: true,
                    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
                CKEDITOR.replace('summaryar', {
                startupFocus: true,
                contentsLangDirection: 'rtl',
                filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form'
                } );
            }
        });
    }
});

$(document).on('click', '#profile-pic-upload', function () {
    $('#profile_pic').click();
});

$(document).on('change', '#profile_pic', function () { 
    var id = $('#id_pg').val();
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    var img_type = $(this).data('type');
    const file = $(this)[0].files[0];
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var _URL = window.URL || window.webkitURL;
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
    imgwidth = this.width;
    imgheight = this.height;
    if (imgwidth >= imgw && imgheight >= imgh){
    readUrl(file, img_type, uploadFile, imgw, imgh, id);
    } else{
    $('input[type="file"]').val('');
    Toast.fire({
    icon: 'error',
            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
    });
    }
    }
    });
    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('id_pg', id);
    formData.append('type', type);
    $.ajax({
    type: "POST",
            url: "{{route('aboutus.upload_image')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            if(type == 'image'){
            $(`#profile_pic_loader`).show();
            }
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#id_pg').val(data.id_pg);
            if(data.type == 'image'){
            $('#slider_en_image').html('<div id="profile-pic-upload" class="add">+</div>' +
                    '<div id="profile_pic_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="slider_en_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-profile" data-type="image" data-id="' + data.id_pg + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>');
                    $('body').addClass('modal-open');
        }
            $('input[type="file"]').val('');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`profile_pic_loader`).hide();
            }
            }
    });
    }
  
    $(document).on('click', '.delete-profile', function () {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });
   
    function delete_image(id, type){
        $.confirm({
            title: '<span class="small">Are you sure to delete this image?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('aboutus.detete_img')}}",
                    data: {
                    id: id,
                    type: type
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    if (data.type == 'image') {
                    $('#slider_en_image').html('<div id="profile-pic-upload" class="add">+</div>' +
                            '<div id="profile_pic_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="slider_en_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="image" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>');
                    }
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    console.log('cancelled');
                    }
            }
    });
    }

</script>