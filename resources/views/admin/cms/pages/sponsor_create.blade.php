<div class="modal-header">
    <h4 class="modal-title"><strong>
            Edit Looking for a Sponsor
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="sponsor_tab">
        <li class="nav-item w-25 "> <a class="nav-link active" href="#page_info1" data-id="{{ isset($id) ? $id  : '' }}" role="tab"><span class="hidden-xs-down">SHOWCASE PROGRAM</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#page_info2" data-id="{{ isset($id) ? $id  : '' }}" role="tab"><span class="hidden-xs-down">WHAT'S IN IT FOR YOU</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#seo" data-id="{{ isset($id) ? $id  : '' }}" role="tab"><span class="hidden-xs-down">SEO</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('admin.cms.pages.sponsor_showcasepm')
</div>


<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #formModal { overflow-y:auto; }
    .area1 {
        height: 80px;
    }
 
</style>

<script>
   $('#page-popup').modal({backdrop: 'static', keyboard: false});

    $(document).on('click', '.save-btn', function () {
        $("#submit_action").val('save');
    });
    $(document).on('click', '.save-and-continue', function () {
        $("#submit_action").val('continue');
    });

    $(document).on('click', '.save-btn1', function () {
        $("#submit_action1").val('save');
    });
    $(document).on('click', '.save-and-continue1', function () {
        $("#submit_action1").val('continue');
    });

    $(document).on('click', '.tab_back', function () {
        
        var id = $(this).data("id");
        var active_tab = $(this).attr("active_tab");
        $.ajax({
            type: "GET",
            url: "{{route('sponsor_tabs')}}",
            data: {'activeTab': active_tab, id: id},
            success: function (result) {
                console.log(result)
                if(active_tab == 'WHATS IN IT FOR YOU'){
                    $('#sponsor_tab a[href="#page_info2"]').tab('show');
                }else{
                    $('#sponsor_tab a[href="#page_info1"]').tab('show');
                }
                
                $('.tab-content').html(result);
            }
        });
    });

    $(document).on('click', '.save-btn-seo', function () {

        $("#frm_create_sponsor_seo").validate({
        ignore: [],
        rules: {
            seo_title_en  : {
                required: true,
            },
            seo_title_ar: {
                required: true,
            },
            seo_content_en  : {
                required: true,
            },
            seo_content_ar: {
                required: true,
            },
        
        },
        
        messages: {
            seo_title_en: {
            required:"Meta Title(EN) required",
            },
            seo_title_ar: {
            required: "Meta Title (AR) required",
            },
            seo_content_en  : {
                required: "Description(EN) required",
            },
            seo_content_ar: {
                required: "Description(AR) required",
            },
        },
            
        
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form) {
            let edit_val=$('#id_pg').val();
            
            if(edit_val){
                $.ajax({
                    type:"POST",
                    url: "{{route('pages.sponsor.seo')}}",
                    data:{
                        seo_title_en:$('#seo_title_en').val(),
                        seo_title_ar:$('#seo_title_ar').val(),
                        seo_content_en:$('#seo_content_en').val(),
                        seo_content_ar:$('#seo_content_ar').val(),
                        id:edit_val

                    },
                    success: function(data){
                        if (data.status == 1) {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            window.location.href = '{{route("admin.pages.get")}}';
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                            $("#contact_tab").tabs({cache: true});
                        }
                        
                        },
                        error: function (err) {
                            
                        }
                });
            }

        }
    });

    });

    $(document).on('click', '.save-and-continue,.save-btn', function () {
    $("#frm_create_sponsor_pageinfo").validate({
        ignore: [],
        rules: {
            title_en  : {
                required: true,
            },
            title_ar: {
                required: true,
            },
            step1_title_en  : {
                required: true,
            },
            step1_title_ar: {
                required: true,
            },
            step1_desc_en  : {
                required: true,
            },
            step1_desc_ar: {
                required: true,
            },
            step2_title_en  : {
            required: true,
            },
            step2_title_ar: {
                required: true,
            },
            step2_desc_en  : {
                required: true,
            },
            step2_desc_ar: {
                required: true,
            },
            step3_title_en  : {
                required: true,
            },
            step3_title_ar: {
                required: true,
            },
            step3_desc_en  : {
                required: true,
            },
            step3_desc_ar: {
                required: true,
            },
        },
        
        messages: {
            title_en: {
                    required:"Title(EN) required",

                    },
            title_ar: {
                    required: "Title (AR) required",
                    },
            step1_title_en  : {
                required: "Subtitle1(EN) required",
            },
            step1_title_ar: {
                required: "Subtitle1(AR) required",
            },
            step1_desc_en  : {
                required: "Subcontent1(EN) required",
            },
            step1_desc_ar: {
                required: "Subcontent1(AR) required",
            },
            step2_title_en  : {
            required: "Subtitle2(EN) required",
            },
            step2_title_ar: {
                required: "Subtitle2(AR) required",
            },
            step2_desc_en  : {
                required: "Subcontent2(EN) required",
            },
            step2_desc_ar: {
                required: "Subcontent2(AR) required",
            },
            step3_title_en  : {
                required: "Subtitle3(EN) required",
            },
            step3_title_ar: {
                required: "Subtitle3(AR) required",
            },
            step3_desc_en  : {
                required: "Subcontent3(EN) required",
            },
            step3_desc_ar: {
                required: "Subcontent3(AR) required",
            },
           
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
            submitHandler: function(form) {
            let edit_val=$('#id_pg').val();
            
            if(edit_val){
                $.ajax({
                    type:"POST",
                    url: "{{route('pages.sponsor.showcase_program')}}",
                    data:$("#frm_create_sponsor_pageinfo").serialize(),
                    success: function(data){
                        if (data.status == 1) {
                            if ($("#submit_action").val() == 'continue') {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                $('#sponsor_tab a[href="#page_info2"]').tab('show');
                                $('.tab-content').html(data.result);
                            } else {
                                
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            }
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                            $("#sponsor_tab").tabs({cache: true});
                        }
                        
                        },
                        error: function (err) {
                            
                        }
                });
            }

        }
    });
});

$(document).on('click', '.save-and-continue1,.save-btn1', function () {
    $("#frm_create_whatinit_pageinfo").validate({
        ignore: [],
        rules: {
            title_en  : {
                required: true,
            },
            title_ar: {
                required: true,
            },
            content_en  : {
                required: true,
            },
            content_ar: {
                required: true,
            },
            step1_title_en  : {
                required: true,
            },
            step1_title_ar: {
                required: true,
            },
            step1_desc_en  : {
                required: true,
            },
            step1_desc_ar: {
                required: true,
            },
            step2_title_en  : {
            required: true,
            },
            step2_title_ar: {
                required: true,
            },
            step2_desc_en  : {
                required: true,
            },
            step2_desc_ar: {
                required: true,
            },
            step3_title_en  : {
                required: true,
            },
            step3_title_ar: {
                required: true,
            },
            step3_desc_en  : {
                required: true,
            },
            step3_desc_ar: {
                required: true,
            },
            step4_title_en  : {
                required: true,
            },
            step4_title_ar: {
                required: true,
            },
            step4_desc_en  : {
                required: true,
            },
            step4_desc_ar: {
                required: true,
            },
        },
        
        messages: {
            title_en: {
                    required:"Title(EN) required",

                    },
            title_ar: {
                    required: "Title (AR) required",
                    },
            content_en  : {
                required: "Content(EN) required",
            },
            content_ar: {
                required: "Content(AR) required",
            },
            step1_title_en  : {
                required: "Subtitle1(EN) required",
            },
            step1_title_ar: {
                required: "Subtitle1(AR) required",
            },
            step1_desc_en  : {
                required: "Subcontent1(EN) required",
            },
            step1_desc_ar: {
                required: "Subcontent1(AR) required",
            },
            step2_title_en  : {
            required: "Subtitle2(EN) required",
            },
            step2_title_ar: {
                required: "Subtitle2(AR) required",
            },
            step2_desc_en  : {
                required: "Subcontent2(EN) required",
            },
            step2_desc_ar: {
                required: "Subcontent2(AR) required",
            },
            step3_title_en  : {
                required: "Subtitle3(EN) required",
            },
            step3_title_ar: {
                required: "Subtitle3(AR) required",
            },
            step3_desc_en  : {
                required: "Subcontent3(EN) required",
            },
            step3_desc_ar: {
                required: "Subcontent3(AR) required",
            },
            step4_title_en  : {
                required: "Subtitle4(EN) required",
            },
            step4_title_ar: {
                required: "Subtitle4(AR) required",
            },
            step4_desc_en  : {
                required: "Subcontent4(EN) required",
            },
            step4_desc_ar: {
                required: "Subcontent4(AR) required",
            },
           
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
            submitHandler: function(form) {
            let edit_val=$('#id_pg').val();
            
            if(edit_val){
                $.ajax({
                    type:"POST",
                    url: "{{route('pages.sponsor.whatinit')}}",
                    data:$("#frm_create_whatinit_pageinfo").serialize(),
                    success: function(data){
                        if (data.status == 1) {
                            if ($("#submit_action1").val() == 'continue') {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                $('#sponsor_tab a[href="#seo"]').tab('show');
                                $('.tab-content').html(data.result);
                            } else {
                                
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            }
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                            $("#sponsor_tab").tabs({cache: true});
                        }
                        
                        },
                        error: function (err) {
                            
                        }
                });
            }

        }
    });
});

$("button[data-dismiss=modal]").click(function () {
    location.reload();
});
$('.nav-tabs a').on('click', function (e) {
    var id = $(this).data("id");
    var x = $(e.target).text();
    var location = $(this).attr('href');
    if (id != '') {
        $.ajax({
            type: "GET",
            url: "{{route('sponsor_tabs')}}",
            data: {'activeTab': x, id: id},
            success: function (result) {
                $('#sponsor_tab a[href="'+location+'"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    }
});

$(document).on('click', '#image1-upload', function () {
    $('#image1_file').click();
});
$(document).on('click', '#image2-upload', function () {
    $('#image2_file').click();
});
$(document).on('click', '#image3-upload', function () {
    $('#image3_file').click();
});
$(document).on('click', '#image4-upload', function () {
    $('#image4_file').click();
});

$(document).on('change', '#image1_file,#image2_file,#image3_file,#image4_file', function () { 
    var id = $('#id_pg').val();
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    var img_type = $(this).data('type');
    const file = $(this)[0].files[0];
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var _URL = window.URL || window.webkitURL;
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
    imgwidth = this.width;
    imgheight = this.height;
    if (imgwidth >= imgw && imgheight >= imgh){
    readUrl(file, img_type, uploadFile, imgw, imgh, id);
    } else{
    $('input[type="file"]').val('');
    Toast.fire({
    icon: 'error',
            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
    });
    }
    }
    });
    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('id_pg', id);
    formData.append('type', type);
    $.ajax({
    type: "POST",
            url: "{{route('sponsor.upload_image')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            if(type == 'image'){
            $(`#image1_loader`).show();
            }
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#id_pg').val(data.id_pg);
            if(data.type == 'sub_image1'){
                $('#sponsor_image1').html('<div id="image1-upload" class="add">+</div>' +
                '<div id="image1_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                '<div class="preview-image-container" id="image1_preview">' +
                '<div class="scrn-link" style="position: relative;top: -20px;">' +
                '<button type="button" class="scrn-img-close delete-image" data-type="sub_image1" data-id="' + data.id_pg + '">' +
                '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                '</div></div>');
            }else if(data.type == 'sub_image2'){
                $('#sponsor_image2').html('<div id="image2-upload" class="add">+</div>' +
                '<div id="image2_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                '<div class="preview-image-container" id="image2_preview">' +
                '<div class="scrn-link" style="position: relative;top: -20px;">' +
                '<button type="button" class="scrn-img-close delete-image" data-type="sub_image2" data-id="' + data.id_pg + '">' +
                '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                '</div></div>');
                
                
            }else if(data.type == 'sub_image3'){
                $('#sponsor_image3').html('<div id="image3-upload" class="add">+</div>' +
                '<div id="image3_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                '<div class="preview-image-container" id="image3_preview">' +
                '<div class="scrn-link" style="position: relative;top: -20px;">' +
                '<button type="button" class="scrn-img-close delete-image" data-type="sub_image3" data-id="' + data.id_pg + '">' +
                '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                '</div></div>');
            }else if(data.type == 'sub_image4'){
                $('#sponsor_image4').html('<div id="image4-upload" class="add">+</div>' +
                '<div id="image4_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                '<div class="preview-image-container" id="image4_preview">' +
                '<div class="scrn-link" style="position: relative;top: -20px;">' +
                '<button type="button" class="scrn-img-close delete-image" data-type="sub_image4" data-id="' + data.id_pg + '">' +
                '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                '</div></div>');
            }
            $('body').addClass('modal-open');
            $('input[type="file"]').val('');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`image1_loader`).hide();
            }
            }
    });
    }
  
    $(document).on('click', '.delete-image', function () {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });
   
    function delete_image(id, type){
        $.confirm({
            title: '<span class="small">Are you sure to delete this image?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('sponsor.detete_img')}}",
                    data: {
                    id: id,
                    type: type
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            Toast.fire({
                            icon: 'success',
                                    title: data.message
                            });
                            if (data.type == 'sub_image1') {
                            $('#sponsor_image1').html('<div id="image1-upload" class="add">+</div>' +
                                    '<div id="image1_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                    '<div class="preview-image-container" style="display: none;" id="image1_preview">' +
                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                    '<button type="button" class="scrn-img-close" data-type="sub_image1" data-id="' + data.id + '">' +
                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                    '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                    '</div></div>');
                            }else if(data.type == 'sub_image2'){
                                $('#sponsor_image2').html('<div id="image2-upload" class="add">+</div>' +
                                    '<div id="image2_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                    '<div class="preview-image-container" style="display: none;" id="image2_preview">' +
                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                    '<button type="button" class="scrn-img-close" data-type="sub_image2" data-id="' + data.id + '">' +
                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                    '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                    '</div></div>');

                            }else if(data.type == 'sub_image3'){
                                $('#sponsor_image3').html('<div id="image3-upload" class="add">+</div>' +
                                    '<div id="image3_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                    '<div class="preview-image-container" style="display: none;" id="image3_preview">' +
                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                    '<button type="button" class="scrn-img-close" data-type="sub_image3" data-id="' + data.id + '">' +
                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                    '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                    '</div></div>');

                            }else if(data.type == 'sub_image4'){
                                $('#sponsor_image4').html('<div id="image4-upload" class="add">+</div>' +
                                    '<div id="image4_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                    '<div class="preview-image-container" style="display: none;" id="image4_preview">' +
                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                    '<button type="button" class="scrn-img-close" data-type="sub_image4" data-id="' + data.id + '">' +
                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                    '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                    '</div></div>');

                            }  
                        }else {
                            Toast.fire({
                            icon: 'error',
                                    title: data.message
                            });
                        }
                    }
            });
            },
                    No: function () {
                    console.log('cancelled');
                    }
            }
    });
    }



</script>