<div class="modal-header">
    <h4 class="modal-title"><strong>
            Edit Contact Us
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="contact_tab">
        <li class="nav-item w-25 "> <a class="nav-link active" href="#page_info" data-id="{{ isset($id) ? $id  : '' }}" role="tab"><span class="hidden-xs-down">PAGE INFO</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#seo" data-id="{{ isset($id) ? $id  : '' }}" role="tab"><span class="hidden-xs-down">SEO</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('admin.cms.pages.contact_pageinfo')
</div>


<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #formModal { overflow-y:auto; }
 
</style>
<script>
    CKEDITOR.replace( 'summaryen', {
        startupFocus: true,
        filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
    CKEDITOR.replace('summaryar', {
    startupFocus: true,
    contentsLangDirection: 'rtl',
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
    } );


</script>

<script>
   $('#page-popup').modal({backdrop: 'static', keyboard: false});

    $(document).on('click', '.save-btn', function () {
        $("#submit_action").val('save');
    });
    $(document).on('click', '.save-and-continue', function () {
        $("#submit_action").val('continue');
    });

    $(document).on('click', '.tab_back', function () {
        
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('contact_tabs')}}",
            data: {'activeTab': 'PAGE INFO', id: id},
            success: function (result) {
                $('#contact_tab a[href="#page_info"]').tab('show');
                $('.tab-content').html(result);

                CKEDITOR.replace( 'summaryen', {
                    startupFocus: true,
                    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
                CKEDITOR.replace('summaryar', {
                startupFocus: true,
                contentsLangDirection: 'rtl',
                filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form'
                } );

            }
        });
    });

    $(document).on('click', '.save-btn-seo', function () {

        $("#frm_create_contact_seo").validate({
        ignore: [],
        rules: {
            seo_title_en  : {
                required: true,
            },
            seo_title_ar: {
                required: true,
            },
            seo_content_en  : {
                required: true,
            },
            seo_content_ar: {
                required: true,
            },
        
        },
        
        messages: {
            seo_title_en: {
            required:"Meta Title(EN) required",
            },
            seo_title_ar: {
            required: "Meta Title (AR) required",
            },
            seo_content_en  : {
                required: "Description(EN) required",
            },
            seo_content_ar: {
                required: "Description(AR) required",
            },
        },
            
        
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form) {
            let edit_val=$('#id_pg').val();
            
            if(edit_val){
                $.ajax({
                    type:"POST",
                    url: "{{route('pages.contact.seo')}}",
                    data:{
                        seo_title_en:$('#seo_title_en').val(),
                        seo_title_ar:$('#seo_title_ar').val(),
                        seo_content_en:$('#seo_content_en').val(),
                        seo_content_ar:$('#seo_content_ar').val(),
                        id:edit_val

                    },
                    success: function(data){
                        if (data.status == 1) {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            window.location.href = '{{route("admin.pages.get")}}';
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                            $("#contact_tab").tabs({cache: true});
                        }
                        
                        },
                        error: function (err) {
                            
                        }
                });
            }

        }
    });

    });

    $(document).on('click', '.save-and-continue,.save-btn', function () {
    $("#frm_create_contact_pageinfo").validate({
        ignore: [],
        rules: {
        title_en  : {
            required: true,
        },
        title_ar: {
            required: true,
        },
        summaryen :{
            required: function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement();
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                return editorcontent.length === 0;
                    }

            },
        summaryar :{
                required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement();
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                    return editorcontent.length === 0;
                },
            },
        address_en  : {
            required: true,
        },
        address_ar: {
            required: true,
        },
        email  : {
            required: true,
        },
        phone: {
            required: true,
        },
        },
        
        messages: {
            title_en: {
                    required:"Title(EN) required",

                    },
            title_ar: {
                    required: "Title (AR) required",
                    },
            summaryen: {
                required: "Short Description(EN) required",
            },
            summaryar: {
                required: "Short Description(AR) required",
            },
            address_en  : {
                required: "Address(EN) required",
            },
            address_ar: {
                required: "Address(AR) required",
            },
            email  : {
                required: "Email required",
            },
            phone: {
                required: "Phone required",
            },
           
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
            submitHandler: function(form) {
            let edit_val=$('#id_pg').val();
            
            if(edit_val){
                $.ajax({
                    type:"POST",
                    url: "{{route('pages.contact.pageinfo')}}",
                    data:{
                        title_en:$('#title_en').val(),
                        title_ar:$('#title_ar').val(),
                        desc_en: CKEDITOR.instances['summaryen'].getData(),
                        desc_ar:CKEDITOR.instances['summaryar'].getData(),
                        address_en:$('#address_en').val(),
                        address_ar:$('#address_ar').val(),
                        email:$('#email').val(),
                        phone:$('#phone').val(),
                        id:edit_val

                    },
                    success: function(data){
                        if (data.status == 1) {
                            if ($("#submit_action").val() == 'continue') {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                $('#contact_tab a[href="#seo"]').tab('show');
                                $('.tab-content').html(data.result);
                            } else {
                                
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            }
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                            $("#contact_tab").tabs({cache: true});
                        }
                        
                        },
                        error: function (err) {
                            
                        }
                });
            }

        }
    });
});

$("button[data-dismiss=modal]").click(function () {
    location.reload();
});
$('.nav-tabs a').on('click', function (e) {
    var id = $(this).data("id");
    var x = $(e.target).text();
    var location = $(this).attr('href');
    if (id != '') {
        $.ajax({
            type: "GET",
            url: "{{route('contact_tabs')}}",
            data: {'activeTab': x, id: id},
            success: function (result) {
                $('#contact_tab a[href="'+location+'"]').tab('show');
                $('.tab-content').html(result);

                CKEDITOR.replace( 'summaryen', {
                    startupFocus: true,
                    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form'
                });
                CKEDITOR.replace('summaryar', {
                startupFocus: true,
                contentsLangDirection: 'rtl',
                filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form'
                } );
            }
        });
    }
});


</script>