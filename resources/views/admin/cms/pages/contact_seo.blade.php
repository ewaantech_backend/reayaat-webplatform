<form id="frm_create_contact_seo" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="row">
                <input type="hidden" id="id_pg" name="id_pg" value="{{$id}}">
                @csrf
                <div class="col-md-6">
                    <label>Meta Title (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="seo_title_en" id="seo_title_en" placeholder="">{{$page->contactSeo[0]->meta_title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Meta Title (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="seo_title_ar" id="seo_title_ar"
                        style="text-align:right !important" placeholder="" >{{$page->contactSeo[1]->meta_title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Description (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="seo_content_en" id="seo_content_en" placeholder="">{{$page->contactSeo[0]->description}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Description (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="seo_content_ar" id="seo_content_ar"
                        style="text-align:right !important" placeholder="" >{{$page->contactSeo[1]->description}}</textarea>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn-seo">
                Save
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{$id}}' href="javascript:void(0);">Back</a>
        </div>
    </div>
</form>
<style>

</style>
