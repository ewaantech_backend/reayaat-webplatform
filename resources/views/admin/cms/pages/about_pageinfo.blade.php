<form id="frm_create_about_pageinfo" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body" style="height: 80vh;overflow-y: auto;">
            <div class="row">
                <input type="hidden" id="id_pg" name="id_pg" value="{{$id}}">
                @csrf
                <div class="col-md-6">
                    <label>Title (EN)</label>
                    <textarea class="form-control area" name="title_en" id="title_en" placeholder="">{{$page->aboutUsPageInfo[0]->title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Title (AR)</label>
                    <textarea class="form-control area" name="title_ar" id="title_ar"
                        style="text-align:right !important" placeholder="" >{{$page->aboutUsPageInfo[1]->title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Content (EN)</label>
                    <textarea class="form-control area" name="content_en" id="content_en" placeholder="">{{$page->aboutUsPageInfo[0]->content}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Content (AR)</label>
                    <textarea class="form-control area" name="content_ar" id="content_ar"
                        style="text-align:right !important" placeholder="" >{{$page->aboutUsPageInfo[1]->content}}</textarea>
                </div>
                <div class="col-md-12">
                    <div class="mb-2">Image</div>
                    <div class="cover-photo" id="slider_en_image">
                        <div id="profile-pic-upload" class="add">+</div>
                        <div id="profile_pic_loader" class="loader" style="display: none;">
                            <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                        </div>
                        <div class="preview-image-container" @if(empty($page->aboutUsPageInfo[0]->image)) style="display:none;" @endif id="slider_en_photo_image_preview">
                            <div class="scrn-link" style="position: relative;top: -20px;">
                                <button type="button" class="scrn-img-close delete-profile" data-type="image" data-id="{{ isset($id) ? $id : '' }}">
                                    <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                </button>
                                <img class="scrn-img" style="max-width: 200px" src="{{ !empty($page->aboutUsPageInfo[0]->image) ? url('uploads/'.$page->aboutUsPageInfo[0]->image) : '' }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="control-fileupload" style="display: none;">
                        <label for="profile_pic" data-nocap="1">Select image:</label>
                        <input type="file" id="profile_pic" name="profile_pic" data-imgw="550" data-imgh="250" data-type="image"/>
                    </div>
                    <div class="mt-2 small">
                        <p>Max file size: 1 MB<br />
                            Supported formats: jpeg, png<br />
                            File dimension: 550 x 250 pixels<br />
                        </p>
                        <span style="display: none;" class="error" id="slider_en_photo-error">Error</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>Sub Title (EN)</label>
                    <textarea class="form-control area" name="subtitle_en" id="subtitle_en" placeholder="">{{$page->aboutUsPageInfo[0]->sub_title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Sub Title (AR)</label>
                    <textarea class="form-control area" name="subtitle_ar" id="subtitle_ar"
                        style="text-align:right !important" placeholder="" >{{$page->aboutUsPageInfo[1]->sub_title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Description (EN)</label>
                    <textarea class="form-control" id="summaryen" name="summaryen" placeholder="">{{$page->aboutUsPageInfo[0]->description}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Description (AR)</label>
                    <textarea class="form-control" id="summaryar" name="summaryar" placeholder="">{{$page->aboutUsPageInfo[1]->description}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Sub Content (EN)</label>
                    <textarea class="form-control area" name="subcont_en" id="subcont_en" placeholder="">{{$page->aboutUsPageInfo[0]->sub_content}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Sub Content (AR)</label>
                    <textarea class="form-control area" name="subcont_ar" id="subcont_ar"
                        style="text-align:right !important" placeholder="" >{{$page->aboutUsPageInfo[1]->sub_content}}</textarea>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="">Back To Listing</a>
        </div>
    </div>
</form>
<div class="modal fade none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-8">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
       CKEDITOR.instances.summaryen.on('change', function() {
            if(CKEDITOR.instances.summaryen.getData().length >  0) {
            $('label[for="summaryen"]').hide();
            }
        });
        CKEDITOR.instances.summaryar.on('change', function() {
            if(CKEDITOR.instances.summaryar.getData().length >  0) {
            $('label[for="summaryar"]').hide();
            }
        });

        $('.change-status').on('click',function(){

            var id = $(this).data("id");
            var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' FAQ',
            content: 'Are you sure to ' + act_value + ' the FAQ?',
            buttons: {
                Yes: function() {
            $.ajax({
                        type:"POST",
                        url: "{{route('admin.faq.status')}}",
                        data:{
                           status:act_value,
                           id: id

                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.faq.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

    

    </script>
@endpush


 
