@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>PAGES</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Title (EN)</th>
                                <th>Template</th>
                                <th width="16%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($page) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($page as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                @if($row_data->template == 1)
                                <td>{{ $row_data->aboutUsPageInfo[0]->title}}</td>
                                @elseif($row_data->template == 2 && $row_data->id == 2)
                                <td>{{ $row_data->termsPageInfo[0]->title}}
                                @elseif($row_data->template == 2 && $row_data->id == 3)
                                <td>{{ $row_data->privacyPageInfo[0]->title}}
                                @elseif($row_data->template == 4)
                                <td>{{ $row_data->contactPageInfo[0]->title}}
                                @elseif($row_data->template == 5)
                                <td>{{ $row_data->sponsorShowcasePm[0]->title}}
                                @endif
                                <td class="right-align">{{ $cms_pages[$row_data->template]}}</td>
                                <td class="text-center">
                                    <!-- <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_page">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button> -->
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit" title="Edit"
                                        data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- popup --}}
<div class="modal fade" id="page-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-height:85%;max-width: 100%">
        <div class="modal-content">
            
        </div>
    </div>
</div>
{{-- endpopup --}}

@endsection

@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    .link_btn{
        cursor: pointer;
    }
    .scrn-img2 {
    width: 150px;
    margin-right: 0px;
    height: 90px;
    transition-duration: 0.5s;
    box-shadow: 0 5px 15px rgb(0 0 0 / 20%);
    border-radius: 6px;
    object-fit: cover;
}
</style>
@endpush

@push('scripts')
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>


<script>

    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});

    $('.page_edit').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('pages.create')}}",
            data: {
                id: id
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#page-popup').modal('show');
            }
        });
    });

  

</script>

@endpush