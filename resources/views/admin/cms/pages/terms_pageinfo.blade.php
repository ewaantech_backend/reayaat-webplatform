<form id="frm_create_terms_pageinfo" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="row">
                <input type="hidden" id="id_pg" name="id_pg" value="{{$id}}">
                @csrf
                <div class="col-md-6">
                    <label>Title (EN)</label>
                    <textarea class="form-control area" name="title_en" id="title_en" placeholder="">{{$page->termsPageInfo[0]->title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Title (AR)</label>
                    <textarea class="form-control area" name="title_ar" id="title_ar"
                        style="text-align:right !important" placeholder="" >{{$page->termsPageInfo[1]->title}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Content (EN)</label>
                    <textarea class="form-control" id="summaryen" name="summaryen" placeholder="">{{$page->termsPageInfo[0]->content}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Content (AR)</label>
                    <textarea class="form-control" id="summaryar" name="summaryar" placeholder="">{{$page->termsPageInfo[1]->content}}</textarea>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="">Back To Listing</a>
        </div>
    </div>
</form>
<style>

</style>
