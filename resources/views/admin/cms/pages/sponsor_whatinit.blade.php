<form id="frm_create_whatinit_pageinfo" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body"style="height: 80vh;overflow-y: auto;" >
            <div class="row">
                <input type="hidden" id="id_pg" name="id_pg" value="{{$id}}">
                @csrf
                <div class="col-md-6">
                <label>Title (EN)</label>
                    <textarea class="form-control area" name="title_en" id="title_en" placeholder="Title (EN)">{{$page->sponsorWhatInIt[0]->title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Title (AR)</label>
                    <textarea class="form-control area" name="title_ar" id="title_ar"
                        style="text-align:right !important" placeholder="Title (AR)" >{{$page->sponsorWhatInIt[1]->title}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Content (EN)</label>
                    <textarea class="form-control area1" name="content_en" id="content_en" placeholder="Content (EN)">{{$page->sponsorWhatInIt[0]->content}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Content (AR)</label>
                    <textarea class="form-control area1" name="content_ar" id="content_ar"
                        style="text-align:right !important" placeholder="Content (AR)" >{{$page->sponsorWhatInIt[1]->content}}</textarea>
                </div>

                <div class="col-md-12">
                <label>Image1</label>
                    <div class="cover-photo" id="sponsor_image1">
                        <div id="image1-upload" class="add">+</div>
                        <div id="image1_loader" class="loader" style="display: none;">
                            <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                        </div>
                        <div class="preview-image-container" @if(empty($page->sponsorWhatInIt[0]->sub_image1)) style="display:none;" @endif id="image1_preview">
                            <div class="scrn-link" style="position: relative;top: -20px;">
                                <button type="button" class="scrn-img-close delete-image" data-type="sub_image1" data-id="{{ isset($id) ? $id : '' }}">
                                    <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                </button>
                                <img class="scrn-img" style="max-width: 200px" src="{{ !empty($page->sponsorWhatInIt[0]->sub_image1) ? url('uploads/'.$page->sponsorWhatInIt[0]->sub_image1) : '' }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="control-fileupload" style="display: none;">
                        <input type="file" id="image1_file" name="image1_file" data-imgw="550" data-imgh="250" data-type="sub_image1"/>
                    </div>
                    <div class="mt-2 small">
                        <p>Max file size: 1 MB<br />
                            Supported formats: jpeg, png<br />
                            File dimension: 550 x 250 pixels<br />
                        </p>
                    </div>
                </div>

                <div class="col-md-6">
                <label>Subtitle1 (EN)</label>
                    <textarea class="form-control area" name="step1_title_en" id="step1_title_en" placeholder="Subtitle1 (EN)">{{$page->sponsorWhatInIt[0]->sub_title1}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle1 (AR)</label>
                    <textarea class="form-control area" name="step1_title_ar" id="step1_title_ar"
                        style="text-align:right !important" placeholder="Subtitle1 (AR)" >{{$page->sponsorWhatInIt[1]->sub_title1}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent1 (EN)</label>
                    <textarea class="form-control area1" name="step1_desc_en" id="step1_desc_en" placeholder="Subcontent1 (EN)">{{$page->sponsorWhatInIt[0]->sub_content1}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent1 (AR)</label>
                    <textarea class="form-control area1" name="step1_desc_ar" id="step1_desc_ar"
                        style="text-align:right !important" placeholder="Subcontent1 (AR)" >{{$page->sponsorWhatInIt[1]->sub_content1}}</textarea>
                </div>
                <div class="col-md-12">
                <label>Image2</label>
                    <div class="cover-photo" id="sponsor_image2">
                        <div id="image2-upload" class="add">+</div>
                        <div id="image2_loader" class="loader" style="display: none;">
                            <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                        </div>
                        <div class="preview-image-container" @if(empty($page->sponsorWhatInIt[0]->sub_image2)) style="display:none;" @endif id="image2_preview">
                            <div class="scrn-link" style="position: relative;top: -20px;">
                                <button type="button" class="scrn-img-close delete-image" data-type="sub_image2" data-id="{{ isset($id) ? $id : '' }}">
                                    <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                </button>
                                <img class="scrn-img" style="max-width: 200px" src="{{ !empty($page->sponsorWhatInIt[0]->sub_image2) ? url('uploads/'.$page->sponsorWhatInIt[0]->sub_image2) : '' }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="control-fileupload" style="display: none;">
                        <input type="file" id="image2_file" name="image2_file" data-imgw="550" data-imgh="250" data-type="sub_image2"/>
                    </div>
                    <div class="mt-2 small">
                        <p>Max file size: 1 MB<br />
                            Supported formats: jpeg, png<br />
                            File dimension: 550 x 250 pixels<br />
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                <label>Subtitle2 (EN)</label>
                    <textarea class="form-control area" name="step2_title_en" id="step2_title_en" placeholder="Subtitle2 (EN)">{{$page->sponsorWhatInIt[0]->sub_title2}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle2 (AR)</label>
                    <textarea class="form-control area" name="step2_title_ar" id="step2_title_ar"
                        style="text-align:right !important" placeholder=">Subtitle2 (AR)" >{{$page->sponsorWhatInIt[1]->sub_title2}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent2 (EN)</label>
                    <textarea class="form-control area1" name="step2_desc_en" id="step2_desc_en" placeholder="Subcontent2 (EN)">{{$page->sponsorWhatInIt[0]->sub_content2}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent2 (AR)</label>
                    <textarea class="form-control area1" name="step2_desc_ar" id="step2_desc_ar"
                        style="text-align:right !important" placeholder="Subcontent2 (AR)" >{{$page->sponsorWhatInIt[1]->sub_content2}}</textarea>
                </div>
                <div class="col-md-12">
                <label>Image3</label>
                    <div class="cover-photo" id="sponsor_image3">
                        <div id="image3-upload" class="add">+</div>
                        <div id="image3_loader" class="loader" style="display: none;">
                            <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                        </div>
                        <div class="preview-image-container" @if(empty($page->sponsorWhatInIt[0]->sub_image3)) style="display:none;" @endif id="image3_preview">
                            <div class="scrn-link" style="position: relative;top: -20px;">
                                <button type="button" class="scrn-img-close delete-image" data-type="sub_image3" data-id="{{ isset($id) ? $id : '' }}">
                                    <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                </button>
                                <img class="scrn-img" style="max-width: 200px" src="{{ !empty($page->sponsorWhatInIt[0]->sub_image3) ? url('uploads/'.$page->sponsorWhatInIt[0]->sub_image3) : '' }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="control-fileupload" style="display: none;">
                        <input type="file" id="image3_file" name="image3_file" data-imgw="550" data-imgh="250" data-type="sub_image3"/>
                    </div>
                    <div class="mt-2 small">
                        <p>Max file size: 1 MB<br />
                            Supported formats: jpeg, png<br />
                            File dimension: 550 x 250 pixels<br />
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                <label>Subtitle3 (EN)</label>
                    <textarea class="form-control area" name="step3_title_en" id="step3_title_en" placeholder="Subtitle3 (EN)">{{$page->sponsorWhatInIt[0]->sub_title3}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle3 (AR)</label>
                    <textarea class="form-control area" name="step3_title_ar" id="step3_title_ar"
                        style="text-align:right !important" placeholder="Subtitle3 (AR)" >{{$page->sponsorWhatInIt[1]->sub_title3}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent3 (EN)</label>
                    <textarea class="form-control area1" name="step3_desc_en" id="step3_desc_en" placeholder="Subcontent3 (EN)">{{$page->sponsorWhatInIt[0]->sub_content3}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent3 (AR)</label>
                    <textarea class="form-control area1" name="step3_desc_ar" id="step3_desc_ar"
                        style="text-align:right !important" placeholder="Subcontent3 (AR)" >{{$page->sponsorWhatInIt[1]->sub_content3}}</textarea>
                </div>
                <div class="col-md-12">
                <label>Image4</label>
                    <div class="cover-photo" id="sponsor_image4">
                        <div id="image4-upload" class="add">+</div>
                        <div id="image4_loader" class="loader" style="display: none;">
                            <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                        </div>
                        <div class="preview-image-container" @if(empty($page->sponsorWhatInIt[0]->sub_image4)) style="display:none;" @endif id="image4_preview">
                            <div class="scrn-link" style="position: relative;top: -20px;">
                                <button type="button" class="scrn-img-close delete-image" data-type="sub_image4" data-id="{{ isset($id) ? $id : '' }}">
                                    <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                </button>
                                <img class="scrn-img" style="max-width: 200px" src="{{ !empty($page->sponsorWhatInIt[0]->sub_image4) ? url('uploads/'.$page->sponsorWhatInIt[0]->sub_image4) : '' }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="control-fileupload" style="display: none;">
                        <input type="file" id="image4_file" name="image4_file" data-imgw="550" data-imgh="250" data-type="sub_image4"/>
                    </div>
                    <div class="mt-2 small">
                        <p>Max file size: 1 MB<br />
                            Supported formats: jpeg, png<br />
                            File dimension: 550 x 250 pixels<br />
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                <label>Subtitle4 (EN)</label>
                    <textarea class="form-control area" name="step4_title_en" id="step4_title_en" placeholder="Subtitle4 (EN)">{{$page->sponsorWhatInIt[0]->sub_title4}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subtitle4 (AR)</label>
                    <textarea class="form-control area" name="step4_title_ar" id="step4_title_ar"
                        style="text-align:right !important" placeholder="Subtitle4 (AR)" >{{$page->sponsorWhatInIt[1]->sub_title4}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent4 (EN)</label>
                    <textarea class="form-control area1" name="step4_desc_en" id="step4_desc_en" placeholder="Subcontent4 (EN)">{{$page->sponsorWhatInIt[0]->sub_content4}}</textarea>
                </div>
                <div class="col-md-6">
                <label>Subcontent4 (AR)</label>
                    <textarea class="form-control area1" name="step4_desc_ar" id="step4_desc_ar"
                        style="text-align:right !important" placeholder="Subcontent (AR)" >{{$page->sponsorWhatInIt[1]->sub_content4}}</textarea>
                </div>
                
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn1">
                Save
            </button>
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue1">
                Save & Continue
            </button>

            <input type="hidden" id="submit_action1" value="" />
            <a class="btn btn-default waves-effect tab_back" data-id='{{$id}}' active_tab="SHOWCASE PROGRAM" href="javascript:void(0);">Back</a>
        </div>
    </div>
</form>
<div class="modal fade none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-8">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style>

</style>
