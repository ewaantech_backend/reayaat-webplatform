@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>SLIDE</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" class="btn btn-info btn-style waves-effect waves-light create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Slide
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="8%">S. No.</th>
                                            <th>Slide ID</th>
                                            <th>Slide Image(EN)</th>
                                            <th>Slide Image(AR)</th>
                                            <th>Slide Title(EN)</th>
                                            <th>Slide Title(AR)</th>
                                            <th width="14%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($slider_data) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($slider_data as $row_data)
                                        
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <td>{{ $row_data->slide_id }}</td>
                                            <td>
                                                @if($row_data->path_en)
                                                <a href="{{ url('uploads/'.$row_data->path_en) }}" target="_blank"><img class="scrn-img2" src="{{ !empty($row_data->path_en) ? url('uploads/'.$row_data->path_en) : '' }}" alt=""></a>
                                                @endif
                                            </td>
                                            <td>
                                                @if($row_data->path_ar)
                                                <a href="{{ url('uploads/'.$row_data->path_ar) }}" target="_blank"><img class="scrn-img2" src="{{ !empty($row_data->path_ar) ? url('uploads/'.$row_data->path_ar) : '' }}" alt=""></a>
                                                @endif
                                            </td>
                                            <td>{{ $row_data->title_en }}</td>
                                            <td>{{ $row_data->title_ar }}</td>
                                            <td class="text-center">
                                               <a class="btn btn-sm btn-success text-white create_btn" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a> 
                                               <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="{{ $row_data['status'] == 'active' ? 'Deactivate Brand' : 'Activate Brand' }}"></div>
                                                </button>
                                                <a class="btn btn-sm btn-danger text-white slide_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="7" class="text-center">No records found!</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $slider_data->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_slider" action="javascript:;" method="POST">
            <div class="modal-content ad-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
<div class="modal fade none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-8">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    .link_btn{
        cursor: pointer;
    }
    .scrn-img2 {
    width: 150px;
    margin-right: 0px;
    height: 90px;
    transition-duration: 0.5s;
    box-shadow: 0 5px 15px rgb(0 0 0 / 20%);
    border-radius: 6px;
    object-fit: cover;
}
</style>
@endpush
@push('scripts')

{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('.create_btn').on('click', function () {
    var slide_id = $(this).data("id");
    $.ajax({
        type: "GET",
        url: "{{route('admin.create_slider')}}",
        data: {'id': slide_id},
        success: function (data) {
            $('.ad-content').html(data);
            $('#formModal').modal('show');
        }
    });
});
$("#frm_create_slider").validate({
    normalizer: function (value) {
        return $.trim(value);
    },
    rules: {
        title_en: {
            required: true,
        },
        title_ar: {
            required: true,
        },
        content_en: {
            required: true,
        },
        content_ar: {
            required: true,
        },
    },
    messages: {
        title_en: {
            required: 'Title(EN) is required.'
        },
        title_ar: {
            required: 'Title(AR) is required.'
        },
        content_en: {
            required: 'Content(EN) is required.'
        },
        content_ar: {
            required: 'Content(AR) is required.'
        },
    },
    submitHandler: function (form) {
        $.ajax({
            type: "POST",
            url: "{{route('admin.save_slider')}}",
            data: $('#frm_create_slider').serialize(),
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if (data.status == 1) {
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    });
                    window.setTimeout(function () {
                        window.location.href = '{{route("admin.slide.get")}}';
                    }, 1000);
                    $("#frm_create_slider")[0].reset();
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
                $('button:submit').attr('disabled', false);
            },
            error: function (err) {
                $('button:submit').attr('disabled', false);
            }
        });
        return false;
    }
});
$('.change-status').on('click', function () {
    var brand_id = $(this).data("id");
    var act_value = $(this).data("activate");
    $.confirm({
        title: act_value + ' Slider',
        content: 'Are you sure to ' + act_value + ' the slider?',
        buttons: {
            Yes: function () {
                $.ajax({
                    type: "POST",
                    url: "{{route('slider.activate')}}",
                    data: {id: brand_id},
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 1000);
//                    window.location.reload();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                });
            },
            No: function () {
                window.location.reload();
            }
        }
    });
});

$('.slide_id_del').on('click',function(){
    var id = $(this).data("id");
    $.confirm({
    title: false,
    content: 'Are you sure to delete this slide ? <br><br>You wont be able to revert this',
    buttons: {
        Yes: function() {

            $.ajax({
                url: "{{route('admin.slide.delete')}}" ,
                type: 'POST',
                data:{
                    id:id
                },
                success: function(data) {
                    if (data.status == 1) {
                        window.setTimeout(function() {
                            window.location.href = '{{route("admin.slide.get")}}';
                        }, 1000);
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
        },
        No: function() {
            console.log('cancelled');
        }
    }
});
});

</script>
@endpush