<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            Slide
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="mb-2">Slide Image (EN)</div>
            <div class="cover-photo" id="slider_en_image">
                <div id="slider-en-upload" class="add">+</div>
                <div id="slider_en_photo_loader" class="loader" style="display: none;">
                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                </div>
                <div class="preview-image-container" @if(empty($row_data->path_en)) style="display:none;" @endif id="slider_en_photo_image_preview">
                     <div class="scrn-link" style="position: relative;top: -20px;">
                        <button type="button" class="scrn-img-close delete-cover-en" data-type="path_en" data-id="{{ isset($row_data->id) ? $row_data->id : '' }}">
                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                        </button>
                        <img class="scrn-img" style="max-width: 200px" src="{{ !empty($row_data->path_en) ? url('uploads/'.$row_data->path_en) : '' }}" alt="">
                    </div>
                </div>
            </div>
            <div class="control-fileupload" style="display: none;">
                <label for="slider_en_photo" data-nocap="1">Select cover photo:</label>
                <input type="file" id="slider_en_photo" name="slider_en_photo" data-imgw="550" data-imgh="250" data-type="path_en"/>
            </div>
            <div class="mt-2 small">
                <p>Max file size: 1 MB<br />
                    Supported formats: jpeg, png<br />
                    File dimension: 550 x 250 pixels<br />
                </p>
                <span style="display: none;" class="error" id="slider_en_photo-error">Error</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mb-2">Slide Image (AR)</div>
            <div class="cover-photo" id="slider_ar_image">
                <div id="slider-ar-upload" class="add">+</div>
                <div id="slider_ar_photo_loader" class="loader" style="display: none;">
                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                </div>
                <div class="preview-image-container" @if(empty($row_data->path_ar)) style="display:none;" @endif id="slider_ar_photo_image_preview">
                     <div class="scrn-link" style="position: relative;top: -20px;">
                        <button type="button" class="scrn-img-close delete-cover-ar" data-type="path_ar" data-id="{{ isset($row_data->id) ? $row_data->id : '' }}">
                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                        </button>
                        <img class="scrn-img" style="max-width: 200px" src="{{ !empty($row_data->path_ar) ? url('uploads/'.$row_data->path_ar) : '' }}" alt="">
                    </div>
                </div>
            </div>
            <div class="control-fileupload" style="display: none;">
                <label for="slider_ar_photo" data-nocap="1">Select cover photo:</label>
                <input type="file" id="slider_ar_photo" name="slider_ar_photo" data-imgw="550" data-imgh="250" data-type="path_ar"/>
            </div>
            <div class="mt-2 small">
                <p>Max file size: 1 MB<br />
                    Supported formats: jpeg, png<br />
                    File dimension: 550 x 250 pixels<br />
                </p>
                <span style="display: none;" class="error" id="slider_ar_photo-error">Error</span>
            </div>
        </div>
        <div class="col-md-6">
            <label class="control-label">Title (EN) <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter title" type="text" name="title_en" id="title_en" value="{{$row_data['title_en'] ?? ''}}">
            <input class="form-control form-white" type="hidden" name="slide_id" id="slide_id" value="{{( isset($row_data['id']) && $row_data['id'] != '' ) ? $row_data['id'] : '' }}">
        </div>
        <div class="col-md-6">
            <label class="control-label">Title (AR) <span class="text-danger">*</span></label>
            <input class="form-control form-white" style="text-align:right !important" placeholder="Enter title" type="text" id="title_ar" name="title_ar" value="{{$row_data['title_ar'] ?? ''}}">
        </div>
        <div class="col-md-6">
            <label class="control-label">Read More Link (EN)</label>
            <input class="form-control form-white" placeholder="" type="text" name="read_more_link_en" id="read_more_link_en" value="{{$row_data['read_more_link_en'] ?? ''}}">
        </div>
        <div class="col-md-6">
            <label class="control-label">Read More Link (AR)</label>
            <input class="form-control form-white" style="text-align:right !important" placeholder="" type="text" id="read_more_link_ar" name="read_more_link_ar"  value="{{$row_data['read_more_link_ar'] ?? ''}}">
        </div>
        <div class="col-md-6">
            <label>Content (EN) <span class="text-danger">*</span></label>
            <textarea class="form-control area" name="content_en" id="content_en">{{$row_data['content_en'] ?? ''}}</textarea>

        </div>
        <div class="col-md-6">
            <label>Content (AR) <span class="text-danger">*</span></label>
            <textarea class="form-control area" name="content_ar" id="content_ar" style="text-align:right !important">{{$row_data['content_ar'] ?? ''}}</textarea>
        </div>
        
        
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" id="sd_id" name="sd_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Update' : 'Create' }}
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>
<script>
    $('#slider-en-upload').click(function (e) {
    $('#slider_en_photo').click();
    });
    $('#slider-ar-upload').click(function (e) {
    $('#slider_ar_photo').click();
    });
    $('#slider_en_photo, #slider_ar_photo').on('change', function () {
    var id = $('#sd_id').val();
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    var img_type = $(this).data('type');
    const file = $(this)[0].files[0];
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var _URL = window.URL || window.webkitURL;
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
    imgwidth = this.width;
    imgheight = this.height;
    if (imgwidth >= imgw && imgheight >= imgh){
    readUrl(file, img_type, uploadFile, imgw, imgh, id);
    } else{
    $('input[type="file"]').val('');
    Toast.fire({
    icon: 'error',
            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
    });
    }
    }
    });
    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('sd_id', id);
    formData.append('type', type);
    $.ajax({
    type: "POST",
            url: "{{route('slider.upload_image')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            if(type == 'path_en'){
            $(`#slider_en_photo_loader`).show();
            }else{
                $(`#slider_ar_photo_loader`).show();
            }
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#sd_id').val(data.sd_id);
            if(data.type == 'path_en'){
            $('#slider_en_image').html('<div id="slider-en-upload" class="add">+</div>' +
                    '<div id="slider_en_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="slider_en_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-cover-en" data-type="path_en" data-id="' + data.sd_id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-cover-en").click(function(e) {' +
                    'delete_image(' + data.sd_id + ' , "path_en");' +
                    '});<\/script>');
                    $('body').addClass('modal-open');
                    $('#slide_id').val(data.sd_id);
        }else{
            $('#slider_ar_image').html('<div id="slider-ar-upload" class="add">+</div>' +
                    '<div id="slider_ar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="slider_ar_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-cover-ar" data-type="path_ar" data-id="' + data.sd_id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-cover-ar").click(function(e) {' +
                    'delete_image(' + data.sd_id + ' , "path_ar");' +
                    '});<\/script>'); 
                    $('body').addClass('modal-open');
                    $('#slide_id').val(data.sd_id);
        }
            $('input[type="file"]').val('');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`slider_en_photo_loader`).hide();
            $(`slider_ar_photo_loader`).hide();
            }
            }
    });
    }
    
    $('.delete-cover-en').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });
    $('.delete-cover-ar').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });
    function delete_image(id, type){
        $.confirm({
            title: '<span class="small">Are you sure to delete this image?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('slider.detete_img')}}",
                    data: {
                    id: id,
                    type: type
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    if (data.type == 'path_en') {
                    $('#slider_en_image').html('<div id="slider-en-upload" class="add">+</div>' +
                            '<div id="slider_en_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="slider_en_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="path_en" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#slider-en-upload").click(function (e) {' +
                            '$("#slider_en_photo").click();});<\/script>');
                    } else
                    {
                    $('#slider_ar_image').html('<div id="slider-ar-upload" class="add">+</div>' +
                            '<div id="slider_ar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="slider_ar_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="path_ar" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#slider-ar-upload").click(function (e) {' +
                            '$("#slider_ar_photo").click();});<\/script>');
                    }
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    console.log('cancelled');
                    }
            }
    });
    }
</script>