@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>FAQs</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "faq_add_btn" class="btn btn-info btn-style waves-effect waves-light create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New FAQ
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->

{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('admin.faq.get')}}" id="search-faq-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search FAQs">
                        </form>

                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault();
                        document.getElementById('search-faq-form').submit();" class="btn btn-info btn-style waves-effect waves-light"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('admin.faq.get')}}" class="btn btn-default btn-outline waves-effect cancel_style">Reset</a>

                    </div>
                </div>
            </div>
        </div>
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th>Question (EN)</th>
                                <th>Answer (EN)</th>
                                <th>Question (AR)</th>
                                <th>Answer (AR)</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($faq) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($faq as $row_data)
                            <tr>
                                <td>{{ $row_data->lang[0]->question ?? '' }}</td>
                                <td>{!!$row_data->lang[0]->answer ?? '' !!}</td>
                                <td>{{ $row_data->lang[1]->question ?? '' }}</td>
                                <td >{!! $row_data->lang[1]->answer ?? '' !!}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit faq_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white faq_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $faq->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- popup --}}
<div class="modal fade" id="faq-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add FAQ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="brdar_photo_loader" class="loader" style="display: none;"> </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-faq" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Question (EN)</label>
                            <textarea class="form-control area" name="title_en" id="title_en" placeholder="Enter your question"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Question (AR)</label>
                            <textarea class="form-control area" name="title_ar" id="title_ar"
                                style="text-align:right !important" placeholder="أدخل سؤالك" ></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Answer (EN)</label>
                            <textarea class="form-control" id="summaryen" name="summaryen" placeholder="Enter answer"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Answer (AR)</label>
                            <textarea class="form-control" id="summaryar" name="summaryar" placeholder="أدخل سؤالك"></textarea>
                        </div>

                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .area{
        height: 80px;
    }
    #hidden_pushnote{
        display: none;
    }
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'summaryen', {
    startupFocus: true,
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});
    CKEDITOR.replace('summaryar', {
    startupFocus: true,
    contentsLangDirection: 'rtl',
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
    } );

$.fn.modal.Constructor.prototype._enforceFocus = function() {
  modal_this = this
  $(document).on('focusin', function (e) {
    if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
    && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select')
    && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
      modal_this.$element.focus()
    }
  })
};
</script>
<script>

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
       CKEDITOR.instances.summaryen.on('change', function() {
        if(CKEDITOR.instances.summaryen.getData().length >  0) {
        $('label[for="summaryen"]').hide();
        }
    });
CKEDITOR.instances.summaryar.on('change', function() {
    if(CKEDITOR.instances.summaryar.getData().length >  0) {
     $('label[for="summaryar"]').hide();
    }
});
        $('#faq_add_btn').on('click',function(e){
            $('#add-faq').trigger("reset")
            CKEDITOR.instances['summaryen'].setData(" ");
            CKEDITOR.instances['summaryar'].setData(" ");
            $('#exampleModalLabel').html('Add FAQ');
            $('#id_pg').val('');
            $('#faq-popup').modal({
                show:true
            })
        })

        $('.faq_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit FAQ');

            page = $(this).data('id')
            var url = "faq/edit/";

            $.get(url  + page, function (data) {

                $('#title_en').val(data.page.lang[0].question);
                $('#title_ar').val(data.page.lang[1].question);
                $('#id_pg').val(data.page.id)
                CKEDITOR.instances['summaryen'].setData(data.page.lang[0].answer);
                CKEDITOR.instances['summaryar'].setData(data.page.lang[1].answer);
                
                $('#faq-popup').modal({
                    show: true

                });
            })
        })

        $('.save_page').on('click',function(e){
            $("#add-faq").validate({
                ignore: [],
                rules: {
                title_en  : {
                    required: true,
                },
                title_ar: {
                    required: true,
                },
              summaryen :{
                    required: function(textarea) {
                     CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                     return editorcontent.length === 0;
                         }

                    },
                summaryar :{
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement();
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                            return editorcontent.length === 0;
                        },
                    },
                },
                messages: {
                title_en: {
                      required:"Question(EN) required",

                      },
                title_ar: {
                      required: "Question (AR) required",
                      },
                summaryen: {
                      required: "Answer (EN) required",
                      },
                summaryar: {
                      required: "Answer(AR) required",
                      }
                 },
                 errorPlacement: function (error, element) {
            if (element.attr("type") == "checkbox") {
                error.insertBefore(element);
            } else {
                error.insertAfter(element);
            }
        },
                 submitHandler: function(form) {
                    $(`#brdar_photo_loader`).show();
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
                    $("input[name='site']:checked").each(function() {
                            test.push($(this).val());
                    });
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('admin.faq.update')}}",
                        data:{
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            content_en: CKEDITOR.instances['summaryen'].getData(),
                            content_ar:CKEDITOR.instances['summaryar'].getData(),
                            id:edit_val

                        },
                        success: function(result){
                            console.log(result.msg)
                            $(`brdar_photo_loader`).hide();
                            $('#faq-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'FAQ updated successfully'
                                });
                                window.location.href = '{{route("admin.faq.get")}}';
                         }
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('admin.faq.store')}}",
                        data:{
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            content_en: CKEDITOR.instances['summaryen'].getData(),
                            content_ar:CKEDITOR.instances['summaryar'].getData(),
                        },

                        success: function(result){
                            console.log(result.msg)
                            $(`brdar_photo_loader`).hide();
                            $('#faq-popup').modal('hide')
                            if(result.msg ==="success")
                                   {
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'FAQ added successfully'
                                    });
                                    window.location.href = '{{route("admin.faq.get")}}';
                                   }else{
                                    Toast.fire({
                                    icon: 'error',
                                    title: 'some errors'
                                    });
                                   }
                             }
                         }) ;
                    }

                }
            });


        });

        $('.change-status').on('click',function(){

            var id = $(this).data("id");
            var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' FAQ',
            content: 'Are you sure to ' + act_value + ' the FAQ?',
            buttons: {
                Yes: function() {
            $.ajax({
                        type:"POST",
                        url: "{{route('admin.faq.status')}}",
                        data:{
                           status:act_value,
                           id: id

                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.faq.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
    $('.faq_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Faq ? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {

                    $.ajax({
                        url: "{{route('admin.faq.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.faq.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });

    </script>
@endpush