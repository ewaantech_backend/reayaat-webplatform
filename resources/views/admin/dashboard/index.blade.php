@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>SUMMARY</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Select date</label>
                    <div class='input-group' id='datepicker'>
                        <input type="text" class="form-control" name="dates">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar icon-style"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-layout-grid2 color-success border-success"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Total Programs Awaiting Approval</div>
                                <div class="stat-digit"><strong>1</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                     <div class="card">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-layout-grid2 color-primary border-primary"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Total Active Programs</div>
                                <div class="stat-digit"><strong>1</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-layout-grid2 color-pink border-pink"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Total Completed Programs</div>
                                <div class="stat-digit"><strong>1</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-layout-grid2 color-danger border-danger"></i></div>
                            <div class="stat-content dib">
                                <div class="stat-text">Total Sponsors</div>
                                <div class="stat-digit"><strong>1</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</div>
@endsection
@push('css')
@php
$rtl_ext = app()->getLocale() == 'en' ? '' : '-rtl';
@endphp
<link href="{{asset('/assets/css/dashboard.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .right-pos{
        position: absolute;
        right: 35px;
        top: 87px;
    }
    .see-more{
        right: 38px;
        position: absolute;
        bottom: 7px;
    }
    .href-style{
        color:  cornflowerblue;
    }
    .card [class*="card-header-"] .card-icon, .card [class*="card-header-"] .card-text {
        padding: 5px;
    }
    .align-p{
        text-align: center;
    }
</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>

$('input[name="dates"]').daterangepicker({maxDate: new Date()});
$('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
let start = picker.startDate.format('YYYY/MM/DD');
let end = picker.endDate.format('YYYY/MM/DD');

});
$('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
window.location.reload();
});
</script>

@endpush