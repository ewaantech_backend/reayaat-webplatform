@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>ADMIN USERS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_user" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New User
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="8%">S. No.</th>
                                <!-- <th>User ID</th> -->
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Role</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($user) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($user as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <!-- <td>{{ $row_data->user_id }}</td> -->
                                <td>{{ $row_data->name }}</td>
                                <td>{{ $row_data->email }}</td>
                                <td> +966 {{ $row_data->phone }}</td>
                                <td>{{ $row_data->roles->name ?? ''}}</td>
                                <td class="text-left">
                                    <a class="btn btn-sm btn-danger text-white password_send" title="Send credentials to user" data-id="{{ $row_data->id }}"><i class="fa fa-envelope"></i></a>
                                    @if($row_data->role_id != 1)
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete user" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $user->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD USER
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="user_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="user_unique" id='user_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Name <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter name" type="text" name="user_name" id="user_name" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Email <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter email address" type="email" id="email" name="email" />
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Phone <span class="text-danger">*</span></label>
                            <input class="form-control form-white" type="text" name="country_code" value="+966" id="country_code" readonly/>
                        </div>
                        <div class="col-md-4">
                            <label>&nbsp;</label>
                            <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone" id="phone" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Role <span class="text-danger">*</span></label>
                            <select class="form-control" name="role" id="role">
                                <option value=""> Select Role </option>
                                @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>
    

    $('#add_new_user').on('click', function() {
     
        $('#name_change').html('Add User');
        $('#save_data').text('Add').button("refresh");
        $("#user_form")[0].reset();
        $('#user_unique').val('');
        $('#user_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-categorys').on('click', function(e) {
        $("#user_form").validate({
            rules: {
                user_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true

                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 6, // will count space
                    maxlength: 12,

                },
                role: {
                    required: true,

                },
               
            },
            messages: {
                user_name: {
                    required: "Name required",
                },
                email: {
                    required: "Email required",
                    email: "Enter valid email"
                },
                phone: {
                    required: "Phone number required",
                    number: "Enter a valid phone number"
                },
                role: {
                    required: 'Role required'

                },
                
            },
            submitHandler: function(form) {
                user_unique = $("#user_unique").val();
                if (user_unique) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.user.update')}}",
                        data: $('#user_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.user.store')}}",
                        data: $('#user_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });

                }



            }
        })
    });

    $('.edit_user').on('click', function(e) {
        userId = $(this).data('id')

        $('#name_change').html('Edit User');
        $('#save_data').text('Save').button("refresh");


        var url = "user/edit/";

        $.get(url + userId, function(data) {
            
            $('#user_name').val(data.user.name),
            $('#email').val(data.user.email),
            $('#phone').val(data.user.phone),
            $('#role').val(data.user.role_id),   
            $('#user_unique').val(data.user.id),
            $('#user_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' User',
            content: 'Are you sure to ' + act_value + ' the user?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('admin.user.status-change')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                    title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                            
                } else {
                    Toast.fire({
                    icon: 'error',
                    title: data.message
                    });
                }
            }
            });
            },
            No: function() {
                    window.location.reload();
                }
            }
        });
    });
   
    $('.password_send').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure you want to send password?',
            buttons: {
                Yes: function() {
           
                    
                    $.ajax({
                        url: "{{route('admin.user.password.send')}}",
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(data) {
                           
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                
                },
         
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
    function deleteUser(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this user? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.user.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.user.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
@endpush