@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>OPPORTUNITY PER YEAR</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Title (EN)</th>
                                <th class="right-align">Title (AR)</th>
                                <th width="16%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($opportunity) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($opportunity as $row_data)
                            <tr>
                              
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->title ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->title ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_page">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit" title="Edit"
                                        data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $opportunity->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- popup --}}
<div class="modal fade" id="page-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Edit Opportunity</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="page-change">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Title (EN)</label>
                            <textarea class="form-control area" name="title_en" id="title_en" ></textarea>

                        </div>
                        <div class="col-md-6">
                            <label>Title (AR)</label>
                            <textarea class="form-control area" name="title_ar" id="title_ar"
                                style="text-align:right !important" ></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Content (EN)</label>
                            <textarea class="form-control" id="summaryen" name="summaryen"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Content (AR)</label>
                            <textarea class="form-control" id="summaryar" name="summaryar"></textarea>
                        </div>
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit" id="save_page" class="btn btn-info waves-effect waves-light">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- endpopup --}}
@endsection
@push('css')
<style>
    .area{
        height: 80px;
    }
</style>
@endpush
@push('scripts')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'summaryen', {
    startupFocus: true,
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});
    CKEDITOR.replace('summaryar', {
    startupFocus: true,
    contentsLangDirection: 'rtl',
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
    } );

$.fn.modal.Constructor.prototype._enforceFocus = function() {
  modal_this = this
  $(document).on('focusin', function (e) {
    if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length 
    && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') 
    && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
      modal_this.$element.focus()
    }
  })
};
</script>
<script>
    
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.page_edit').on('click', function(e) {
        e.preventDefault();
        page = $(this).data('id')
        var url = "opportunity/edit/";
        
       $.get(url  + page, function (data) {
           $('#title_en').val(data.opportunity.lang[0].title);
            $('#title_ar').val(data.opportunity.lang[1].title ?? '');
            $('#id_pg').val(data.opportunity.id)
            
            CKEDITOR.instances['summaryen'].setData(data.opportunity.lang[0].content);
            CKEDITOR.instances['summaryar'].setData(data.opportunity.lang[1].content);
           $('#page-popup').modal({
            show: true

            });
       }) 
        
       
    });
    CKEDITOR.instances.summaryen.on('change', function() { 	
        if(CKEDITOR.instances.summaryen.getData().length >  0) {
        $('label[for="summaryen"]').hide();
        }
    });
CKEDITOR.instances.summaryar.on('change', function() { 	
    if(CKEDITOR.instances.summaryar.getData().length >  0) {
     $('label[for="summaryar"]').hide();
    }
});
    $('#save_page').on('click',function(e){
              
        $("#page-change").validate({
            ignore: [],
        rules: {
            title_en : {
                required: true,
                // minlength: 5
            },
            title_ar: {
                required: true
            },
            summaryen :{
                    required: function(textarea) {
                     CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                     return editorcontent.length === 0;
                         }
                       
                    },
                summaryar :{
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement();
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                            return editorcontent.length === 0;
                        },
                    },

        },
        messages: {
            title_en: "Title (EN) required",
            title_ar: "Title (AR) required",
            summaryen: {
                      required: "Content  (EN) required",
                      },
                summaryar: {
                      required: "Content (AR) required",
                      }
           },
           submitHandler: function(form) {
            check_val =$("#hidden_push_note").is(":checked") ? 'yes':'no'
             $.ajax({
             type:"POST",
             url: "{{route('admin.opportunity.update')}}",
             data:{ 
                 title_en:$('#title_en').val(),
                 title_ar:$('#title_ar').val(),
                 id:$('#id_pg').val(),
                 content_en: CKEDITOR.instances['summaryen'].getData(),
                 content_ar:CKEDITOR.instances['summaryar'].getData(),
                 push_val : check_val
        
             },

             success: function(result){
                 $('#page-popup').modal('hide');
                 Toast.fire({
                     icon: 'success',
                     title: 'Opportunity updated  successfully'
                 });
                 window.location.reload();
             }
         })
      }
});
    });

    $('.change-status').on('click',function(){
            
            var id = $(this).data("id");
            var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Opportunity',
            content: 'Are you sure to ' + act_value + ' the Opportunity?',
            buttons: {
                Yes: function() {
            $.ajax({
                        type:"POST",
                        url: "{{route('admin.opportunity.status')}}",
                        data:{ 
                           status:act_value,
                           id: id
                   
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.opportunity.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
  
</script>

@endpush