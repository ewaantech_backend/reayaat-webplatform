@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>TYPE</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_type" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Type
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('admin.type.get')}}" id="search-type-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search Types">
                        </form>
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault();
                        document.getElementById('search-type-form').submit();" class="btn btn-info btn-style waves-effect waves-light"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('admin.type.get')}}" class="btn btn-default btn-outline waves-effect cancel_style">Reset</a>

                    </div>
                </div>
            </div>
        </div>
        
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="8%">S. No.</th>
                                <th>Type (EN)</th>
                                <th>Type (AR)</th>
                                <th>Parent Type</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($type) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($type as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name }}</td>
                                <td>{{ $row_data->lang[1]->name }}</td>
                                <td>{{ $row_data->getParentsNames() }}</td>
                                <td class="text-left">
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Type" onclick="deleteType({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                  

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $type->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="type_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD Type
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="type_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="type_id" id='type_id'>
                        <div class="col-md-6">
                            <label class="control-label">Type (EN) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter type" type="text" name="type_name_en" id="type_name_en" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Type (AR) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" style="text-align:right !important" placeholder="Enter type" type="text" id="type_name_ar" name="type_name_ar" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Category <span class="text-danger">*</span></label>
                            <select class="form-control" name="category" id="category">
                                <option value=""> Select Category </option>
                                @foreach($category as $cat)
                                <option value="{{$cat->lang[0]->category_id}}">{{$cat->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Parent Type</label>
                            <select class="form-control" name="parent_type" id="parent_type">
                                <option value=""> Select Parent Type </option>
                                @foreach($parent_type as $type)
                                <option value="{{$type->lang[0]->type_id}}">{{$type->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-type" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>

    $('#add_new_type').on('click', function() {
        $('#name_change').html('Add Type');
        $('#save_data').text('Add').button("refresh");
        $("#type_form")[0].reset();
        $('#type_id').val('');
        $('#type_popup').modal({
            show: true
        });
        $("#parent_type > option").each(function() {
            $(this).show();
        });
        $("#parent_type").prop('disabled',false);
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-type').on('click', function(e) {
        $("#type_form").validate({
            rules: {
                type_name_en: {
                    required: true,
                },
                type_name_ar: {
                    required: true,

                },
                category: {
                    required: true,

                },
            },
            messages: {
                type_name_en: {
                    required: "Type (EN) required",
                },
                type_name_ar: {
                    required: "Type (AR) required",
                },
                category: {
                    required: "Category required",
                },
            },
            submitHandler: function(form) {
                type_id = $("#type_id").val();
                if (type_id) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.type.store')}}",
                        data: $('#type_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#type_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.type.store')}}",
                        data: $('#type_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#type_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });

                }



            }
        })
    });

    $('.edit_user').on('click', function(e) {
        typeId = $(this).data('id')

        $('#name_change').html('Edit Type');
        $('#save_data').text('Save').button("refresh");
        var url = "type/edit/";

        $.get(url + typeId, function(data) {
            
            $('#type_name_en').val(data.type.lang[0].name),
            $('#type_name_ar').val(data.type.lang[1].name),
            $('#type_id').val(data.type.id)
            $('#category').val(data.type.category_id)
            $('#parent_type').val(data.type.parent_id)
            
            $('#type_popup').modal({
                show: true

            });

            $("#parent_type > option").each(function() {
                $(this).show();
            });

            if(data.type.is_parent == 'yes'){
                $("#parent_type > option").each(function() {
                    if(this.value == typeId){
                        $(this).hide();
                    }
                });
            }

            if(data.sub_type > 0){
                $("#parent_type").prop('disabled','disabled');
            }else{
                $("#parent_type").prop('disabled',false);
            }
            
        });
    });

    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' Type',
            content: 'Are you sure to ' + act_value + ' the type?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('admin.type.status-change')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                    title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                            
                } else {
                    Toast.fire({
                    icon: 'error',
                    title: data.message
                    });
                }
            }
            });
            },
            No: function() {
                    window.location.reload();
                }
            }
        });
    });

    function deleteType(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this type? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.type.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.type.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
@endpush