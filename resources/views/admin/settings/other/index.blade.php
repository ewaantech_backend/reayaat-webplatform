@extends('layouts.master')
@section('content')
@include('auth.flash-msg')

<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>OTHER SETTINGS</h1>
                        </div>
                    </div>
                </div>
            </div>
<div class="row">
    <div class="col-md-12">

        <div class="card-body">
            <form id="name_reset_form" action="{{route('admin.other.store')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="basic-form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Currency (EN)</label>
                                        @if(!empty($other->currency))
                                            <input type="text" class="form-control" id="currency_en"
                                                name="currency_en" value="{{ old('currency_en',json_decode($other->currency)->currency_en) }} " placeholder="Enter Currency">
                                                @else
                                                <input type="text" class="form-control" id="currency_en"
                                                name="currency_en" placeholder="Enter Currency" value="{{ old('currency_en') }}">
                                                @endif
                                                @error('currency_en')
                                                <small class="error">
                                                    <strong>{{ $message }}</strong>
                                                </small>

                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label>Currency (AR)</label>
                                            @if(!empty($other->currency))
                                            <input type="text" class="form-control text-right" id="currency_ar"
                                                name="currency_ar" placeholder="أدخل العملة" value="{{ old('currency_en',json_decode($other->currency)->currency_ar) }}">
                                                @else
                                                <input type="text" placeholder="أدخل العملة" class="form-control text-right" id="currency_ar"
                                                name="currency_ar" value="{{ old('currency_ar') }}">
                                                @endif
                                                @error('currency_ar')
                                                <small class="error">
                                                    <strong>{{ $message }}</strong>
                                                </small>

                                            @enderror
                                        </div>
                                        <div class="hr col-md-12"></div>
                                        <div class=" col-md-12"></div>
                                        <div class="col-md-12 bottm">
                                            <div>
                                            <label class="control-label label_inline">Notify </label>
                                            <input type="text" class="form-control form-control-inline ml-3" id="notify_days_yearly_pkg" name="notify_days_yearly_pkg" value="{{ !empty($other->notify_days_yearly_pkg)? $other->notify_days_yearly_pkg : "" }}">
                                            <label class="control-label"> days prior in yearly package</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 bottm hr">
                                            <div>
                                            <label class="control-label label_inline">Notify </label>
                                            <input type="text" class="form-control form-control-inline ml-3" id="notify_days_monthly_pkg" name="notify_days_monthly_pkg" value="{{ !empty($other->notify_days_monthly_pkg)? $other->notify_days_monthly_pkg : "" }}">
                                            <label class="control-label"> days prior in monthly package</label>
                                            </div>
                                        </div>
                                
                                        <div class="col-md-6 ">
                                            <div class=" col-md-12"></div>
                                            <span>
                                                <label class="mrgn bottm">Opportunity per year display </label>
                                            </span>
                                            <span style="float:right;">
                                                <label class="switch">
                                                    <input type="checkbox" id = "opportunity_per_year_display"  value = "Y" name="opportunity_per_year_display" @if(!empty($other->opportunity_per_year_display)){{ $other->opportunity_per_year_display == "enable" ? 'checked' : ''}} @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class=" col-md-12"></div>
                                            <span>
                                                <label class="mrgn bottm">Registered views display</label>
                                            </span>
                                            <span style="float:right;">
                                                <label class="switch">
                                                    <input type="checkbox" id = "registered_views_display"  value = "Y" name="registered_views_display" @if(!empty($other->registered_views_display)){{ $other->registered_views_display == "enable" ? 'checked' : ''}} @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-6 ">
                                            <span>
                                                <label class="mrgn bottm">Testimonial display</label>
                                            </span>
                                            <span style="float:right;">
                                                <label class="switch">
                                                    <input type="checkbox" id = "testimonial_display"  value = "Y" name="testimonial_display" @if(!empty($other->testimonial_display)){{ $other->testimonial_display == "enable" ? 'checked' : ''}} @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-6 ">
                                            <span>
                                                <label class="mrgn bottm">Opportunities display</label>
                                            </span>
                                            <span style="float:right;">
                                                <label class="switch">
                                                    <input type="checkbox" id = "opportunities_display"  value = "Y" name="opportunities_display" @if(!empty($other->opportunities_display)){{ $other->opportunities_display == "enable" ? 'checked' : ''}} @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-6 ">
                                            <span>
                                                <label class="mrgn bottm">Sponsor display</label>
                                            </span>
                                            <span style="float:right;">
                                                <label class="switch">
                                                    <input type="checkbox" id = "sponsor_display"  value = "Y" name="sponsor_display" @if(!empty($other->sponsor_display)){{ $other->sponsor_display == "enable" ? 'checked' : ''}} @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-6 ">
                                            <span>
                                                <label class="mrgn bottm">Connection mode display</label>
                                            </span>
                                            <span style="float:right;">
                                                <label class="switch">
                                                    <input type="checkbox" id = "connection_mode_display"  value = "Y" name="connection_mode_display" @if(!empty($other->connection_mode_display)){{ $other->connection_mode_display == "enable" ? 'checked' : ''}} @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-6 ">
                                            <span>
                                                <label class="mrgn bottm">Total event profile viewed display</label>
                                            </span>
                                            <span style="float:right;">
                                                <label class="switch">
                                                    <input type="checkbox" id = "total_event_profile_viewed_display"  value = "Y" name="total_event_profile_viewed_display" @if(!empty($other->total_event_profile_viewed_display)){{ $other->total_event_profile_viewed_display == "enable" ? 'checked' : ''}} @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="hr col-md-12"></div>
                                        <div class=" col-md-12"></div>
                                        <div class="col-md-6 ">
                                            <h6 class="mrgn bottm" style="color:#000">Social Medial Links</h6>
                                            <label class="mrgn bottm">Facebook</label>
                                            <input type="text" class="form-control" id="facebook_link"
                                            name="facebook_link" placeholder="Add link here" value="{{ !empty($other->facebook_link)? $other->facebook_link : "" }}">
                                        </div>
                                        
                                        <div class="col-md-6 ">
                                            <h6 class="mrgn bottm"></h6>
                                            <label class="mrgn bottm">LinkedIn</label>
                                            <input type="text" class="form-control" id="linkedin_link"
                                            name="linkedin_link" placeholder="Add link here" value="{{ !empty($other->linkedin_link)? $other->linkedin_link : "" }}">
                                        </div>
                                        <div class="col-md-6 ">
                                            <label class="mrgn bottm">Instagram</label>
                                            <input type="text" class="form-control" id="instagram_link"
                                            name="instagram_link" placeholder="Add link here" value="{{ !empty($other->instagram_link)? $other->instagram_link : "" }}">
                                        </div>
                                        <div class="col-md-6 ">
                                            <label class="mrgn bottm">Twitter</label>
                                            <input type="text" class="form-control" id="twitter_link"
                                            name="twitter_link" placeholder="Add link here" value="{{ !empty($other->twitter_link)? $other->twitter_link : "" }}">
                                        </div>
                                        <div class="hr col-md-12"></div>
                                        <div class=" col-md-12"></div>
                                        <div class="col-md-6 ">
                                            <label class="mrgn bottm">Footer Tag Copyright</label>
                                            <input type="text" class="form-control" id="copyright_tag"
                                            name="copyright_tag" placeholder="Copyright @ 2021 Reayaat.com" value="{{ !empty($other->copyright_tag)? $other->copyright_tag : "" }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row justify-content-end pb-5">
                                    <div class="col-md-12 filter-by-boat-jalbooth-butn01 text-md-left">
                                        <button type="submit" class="btn btn-info btn-style waves-effect waves-light">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection
@push('css')
<style>
    .switch {
  position: relative;
  display: inline-block;
     margin: 0 0.5rem;
    padding: 0;
    position: relative;
    border: none;
    height: 1.5rem;
    width: 3rem;
    border-radius: 1.5rem;
    margin-right: 50px;
}
label {
        font-weight: 500;
        margin-bottom: 16px;
        font-family: 'Roboto', sans-serif;
        font-size: 15px;
        color: #373757;
       
    }
    .bottm
    {
        padding-bottom: 37px;
    }

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: -2px;
  right: 0;
  bottom: 0;
  background-color:#bdc1c8;
  -webkit-transition: .4s;
  transition: .4s;
}
.style_labl{
    font-weight: 400;
    margin-bottom: 16px;
    font-family: 'Roboto', sans-serif;
    font-size: 11px;
    color: #373757;
}

.slider:before {
  position: absolute;
  content: "";
 
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;

      position: absolute;
    top: 0.1875rem;
    left: 0.1875rem;
    width: 1.125rem;
    height: 1.125rem;
    border-radius: 1.125rem;
    background: #fff;
}

input:checked + .slider {
  background-color: #002e59;
}

input:focus + .slider {
  box-shadow: 0 0 1px #002e59;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.col-md-12.row.form_first {
    margin-left: -15px;
}
label.mrgn {
    margin-bottom: -10px;
}

    .form-control{
        width: 60%;
    }
    

    .error {
        color: red;
    }
    #clear-btn {
        top: 3px !important;
    }
    .reset_style{
        margin-left:15px;
    }
    
   .form-control-inline {
    width:60px !important;
    display: inline;
}
.label_inline {
    margin-right: -15px !important;
}
.btn-toggle.active {
    background-color: #002e59;
}
input[type=checkbox],input[type=radio]{
        box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px;
    }
.radio_style{
    margin-top: 12px;
}
.cls_payment{
    margin-right: 10px !important; 
}

.hr {
    border: 1px dotted #ddd;
    border-style: none none dotted; 
    color: #fff; 
    background-color: #fff;
}
</style>

@endpush
@push('scripts')
@push('scripts')
<script>

if ($("#name_reset_form").length > 0) {
    
 $.validator.addMethod('minStrict', function (value, el, param) {
        return this.optional( el ) || value >= param && !(value % 1);
    });
        $("#name_reset_form").validate({
 
            rules: {
                currency_en: {
                    required: true,
                   
                },
                currency_ar: {
                    required: true,
                },
                notify_days_yearly_pkg: {
                    required: true,
                    number: true
                },
                notify_days_monthly_pkg: {
                    required: true,
                    number: true
                },
              
                facebook_link:{
                    required:true
                },
                linkedin_link: {
                    required: true,
                },
                instagram_link:{
                    required:true
                },
                twitter_link: {
                    required: true,
                },
                copyright_tag:{
                    required:true
                },

            },
            messages: {
 
                currency_en: {
                    required: "Currency in EN is required",
                },
                currency_ar: {
                    required: "Currency in AR is required",
                },
                notify_days_yearly_pkg: {
                    required: "Notify days yearly package is required",
                    number:"Please enter a valid number"
                },
                notify_days_monthly_pkg: {
                    required: "Notify days monthly package is required",
                    number:"Please enter a valid number"
                },
                facebook_link: {
                    required: "Facebook is required",
                },
                linkedin_link: {
                    required: "LinkedIn is required",
                },
                instagram_link: {
                    required: "Instagram is required",
                },
                twitter_link: {
                    required: "Twitter is required",
                },
                copyright_tag: {
                    required: "Copyright is required",
                },
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "notify_days_yearly_pkg" || element.attr("name") == "notify_days_monthly_pkg") {
                    error.insertAfter( element.parent());
                }else{
                    error.insertAfter(element);
                }
            },
        })
    }
 
</script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?libraries=places,geocoder,drawing&key=AIzaSyBG_t-XkmJAtPKIpyhOoiXz6QuXphkaBQI&callback=initMap">
</script>


@endpush