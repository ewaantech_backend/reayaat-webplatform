@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>CATEGORY</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_user" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Category
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="8%">S. No.</th>
                                <th>Category (EN)</th>
                                <th>Category (AR)</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($category) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($category as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name }}</td>
                                <td>{{ $row_data->lang[1]->name }}</td>
                                <td class="text-left">
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Category" onclick="deleteCategory({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                  

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $category->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="cat_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD Category
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="cat_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="category_id" id='category_id'>
                        <div class="col-md-6">
                            <label class="control-label">Category (EN) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter category" type="text" name="cat_name_en" id="cat_name_en" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Category (AR) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" style="text-align:right !important" placeholder="Enter category" type="text" id="cat_name_ar" name="cat_name_ar" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>

    $('#add_new_user').on('click', function() {
        $('#name_change').html('Add Category');
        $('#save_data').text('Add').button("refresh");
        $("#cat_form")[0].reset();
        $('#category_id').val();
        $('#cat_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-categorys').on('click', function(e) {
        $("#cat_form").validate({
            rules: {
                cat_name_en: {
                    required: true,
                },
                cat_name_ar: {
                    required: true,

                },
            },
            messages: {
                cat_name_en: {
                    required: "Category (EN) required",
                },
                cat_name_ar: {
                    required: "Category (AR) required",
                },
            },
            submitHandler: function(form) {
                category_id = $("#category_id").val();
                if (category_id) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.category.store')}}",
                        data: $('#cat_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#cat_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.category.store')}}",
                        data: $('#cat_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#cat_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });

                }



            }
        })
    });

    $('.edit_user').on('click', function(e) {
        catId = $(this).data('id')

        $('#name_change').html('Edit Category');
        $('#save_data').text('Save').button("refresh");
        var url = "category/edit/";

        $.get(url + catId, function(data) {
            
            $('#cat_name_en').val(data.category.lang[0].name),
            $('#cat_name_ar').val(data.category.lang[1].name),
            $('#category_id').val(data.category.id)
            $('#cat_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' Category',
            content: 'Are you sure to ' + act_value + ' the category?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('admin.category.status-change')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                    title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                            
                } else {
                    Toast.fire({
                    icon: 'error',
                    title: data.message
                    });
                }
            }
            });
            },
            No: function() {
                    window.location.reload();
                }
            }
        });
    });

    function deleteCategory(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this category? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.category.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.category.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
@endpush