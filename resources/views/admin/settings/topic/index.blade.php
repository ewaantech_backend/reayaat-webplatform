@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>TOPIC</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_type" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Topic
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('admin.topic.get')}}" id="search-type-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search Topics">
                        </form>
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault();
                        document.getElementById('search-type-form').submit();" class="btn btn-info btn-style waves-effect waves-light"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('admin.topic.get')}}" class="btn btn-default btn-outline waves-effect cancel_style">Reset</a>

                    </div>
                </div>
            </div>
        </div>
        
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="8%">S. No.</th>
                                <th>Topic (EN)</th>
                                <th>Topic (AR)</th>
                                <th>Parent Topic</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($topic) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($topic as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name }}</td>
                                <td>{{ $row_data->lang[1]->name }}</td>
                                <td>{{ $row_data->getParentsNames() }}</td>
                                <td class="text-left">
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Type" onclick="deleteTopic({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                  

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $topic->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="topic_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD Topic
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="type_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="topic_id" id='topic_id'>
                        <div class="col-md-6">
                            <label class="control-label">Topic (EN) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter topic" type="text" name="topic_name_en" id="topic_name_en" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Topic (AR) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" style="text-align:right !important" placeholder="Enter topic" type="text" id="topic_name_ar" name="topic_name_ar" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Category <span class="text-danger">*</span></label>
                            <select class="form-control" name="category" id="category">
                                <option value=""> Select Category </option>
                                @foreach($category as $cat)
                                <option value="{{$cat->lang[0]->category_id}}">{{$cat->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Parent Topic</label>
                            <select class="form-control" name="parent_topic" id="parent_topic">
                                <option value=""> Select Parent Topic </option>
                                @foreach($parent_topic as $topic)
                                <option value="{{$topic->lang[0]->topic_id}}">{{$topic->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-type" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>

    $('#add_new_type').on('click', function() {
        $('#name_change').html('Add Topic');
        $('#save_data').text('Add').button("refresh");
        $("#type_form")[0].reset();
        $('#topic_id').val('');
        $('#topic_popup').modal({
            show: true
        });
        $("#parent_topic > option").each(function() {
            $(this).show();
        });
        $("#parent_topic").prop('disabled',false);
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-type').on('click', function(e) {
        $("#type_form").validate({
            rules: {
                topic_name_en: {
                    required: true,
                },
                topic_name_ar: {
                    required: true,

                },
                category: {
                    required: true,

                },
            },
            messages: {
                topic_name_en: {
                    required: "Topic (EN) required",
                },
                topic_name_ar: {
                    required: "Topic (AR) required",
                },
                category: {
                    required: "Category required",
                },
            },
            submitHandler: function(form) {
                topic_id = $("#topic_id").val();
                if (topic_id) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.topic.store')}}",
                        data: $('#type_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#type_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.topic.store')}}",
                        data: $('#type_form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#type_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });

                }



            }
        })
    });

    $('.edit_user').on('click', function(e) {
        topicId = $(this).data('id')

        $('#name_change').html('Edit Topic');
        $('#save_data').text('Save').button("refresh");
        var url = "topic/edit/";

        $.get(url + topicId, function(data) {
            
            $('#topic_name_en').val(data.topic.lang[0].name),
            $('#topic_name_ar').val(data.topic.lang[1].name),
            $('#topic_id').val(data.topic.id)
            $('#category').val(data.topic.category_id)
            $('#parent_topic').val(data.topic.parent_id)
            
            $('#topic_popup').modal({
                show: true

            });

            $("#parent_topic > option").each(function() {
                $(this).show();
            });

            if(data.topic.is_parent == 'yes'){
                $("#parent_topic > option").each(function() {
                    if(this.value == topicId){
                        $(this).hide();
                    }
                });
            }

            if(data.sub_topic > 0){
                $("#parent_topic").prop('disabled','disabled');
            }else{
                $("#parent_topic").prop('disabled',false);
            }
            
        });
    });

    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' Type',
            content: 'Are you sure to ' + act_value + ' the type?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('admin.topic.status-change')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                    title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                            
                } else {
                    Toast.fire({
                    icon: 'error',
                    title: data.message
                    });
                }
            }
            });
            },
            No: function() {
                    window.location.reload();
                }
            }
        });
    });

    function deleteTopic(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this topic? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.topic.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.topic.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
@endpush