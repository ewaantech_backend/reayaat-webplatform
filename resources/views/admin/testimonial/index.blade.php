@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>TESTIMONIALS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "testimonial_add_btn" class="btn btn-info btn-style waves-effect waves-light create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Testimonial
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('admin.testimonial.get')}}" id="search-testimonial-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search by Testimonial/Name/Company">
                        </form>

                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault();
                        document.getElementById('search-testimonial-form').submit();" class="btn btn-info btn-style waves-effect waves-light"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('admin.testimonial.get')}}" class="btn btn-default btn-outline waves-effect cancel_style">Reset</a>

                    </div>
                </div>
            </div>
        </div>
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Name (EN)</th>
                                <th>Testimonial (EN)</th>
                                <th>Name (AR)</th>
                                <th>Testimonial (AR)</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($testimonial) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($testimonial as $row_data)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{!!$row_data->lang[0]->testimonial ?? '' !!}</td>
                                <td>{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td >{!! $row_data->lang[1]->testimonial ?? '' !!}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit testimonial_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white testimonial_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $testimonial->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- popup --}}
<div class="modal fade" id="testimonial-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Testimonial</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="brdar_photo_loader" class="loader" style="display: none;"> </div>
            <div class="modal-body" data-no-padding="no-padding" style="height: 80vh;overflow-y: auto;">
                <form id="add-testimonial" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Name (EN)</label>
                            <input class="form-control form-white" name="name_en" id="name_en" placeholder="Enter name"/>
                        </div>
                        <div class="col-md-6">
                            <label>Name (AR)</label>
                            <input class="form-control form-white" name="name_ar" id="name_ar"
                                style="text-align:right !important" placeholder="أدخل الاسم" />
                        </div>
                        <div class="col-md-6">
                            <label>Designation (EN)</label>
                            <input class="form-control form-white" name="designation_en" id="designation_en" placeholder="Enter designation"/>
                        </div>
                        <div class="col-md-6">
                            <label>Designation (AR)</label>
                            <input class="form-control form-white" name="designation_ar" id="designation_ar"
                                style="text-align:right !important" placeholder="أدخل التسمية" />
                        </div>
                        <div class="col-md-6">
                            <label>Company (EN)</label>
                            <input class="form-control form-white" name="company_en" id="company_en" placeholder="Enter company"/>
                        </div>
                        <div class="col-md-6">
                            <label>Company (AR)</label>
                            <input class="form-control form-white" name="company_ar" id="company_ar"
                                style="text-align:right !important" placeholder="أدخل الشركة" />
                        </div>
                        
                        <div class="col-md-6">
                            <label>Testimonial (EN)</label>
                            <textarea class="form-control" id="summaryen" name="summaryen" placeholder="Enter testimonial"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Testimonial (AR)</label>
                            <textarea class="form-control" id="summaryar" name="summaryar" placeholder="أدخل الشهادة"></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-2">Profile photo</div>
                            <div class="cover-photo" id="slider_en_image">
                                <div id="profile-pic-upload" class="add">+</div>
                                <div id="profile_pic_loader" class="loader" style="display: none;">
                                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                </div>
                                <div class="preview-image-container" id="slider_en_photo_image_preview">
                                    <div class="scrn-link" style="position: relative;top: -20px;">
                                        <button type="button" class="scrn-img-close delete-profile" data-type="image" data-id="">
                                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                        </button>
                                        <img class="scrn-img" style="max-width: 200px" src="" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="control-fileupload" style="display: none;">
                                <label for="profile_pic" data-nocap="1">Select profile photo:</label>
                                <input type="file" id="profile_pic" name="profile_pic" data-imgw="550" data-imgh="250" data-type="image"/>
                            </div>
                            <div class="mt-2 small">
                                <p>Max file size: 1 MB<br />
                                    Supported formats: jpeg, png<br />
                                    File dimension: 550 x 250 pixels<br />
                                </p>
                                <span style="display: none;" class="error" id="slider_en_photo-error">Error</span>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-8">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    .link_btn{
        cursor: pointer;
    }
    .scrn-img2 {
    width: 150px;
    margin-right: 0px;
    height: 90px;
    transition-duration: 0.5s;
    box-shadow: 0 5px 15px rgb(0 0 0 / 20%);
    border-radius: 6px;
    object-fit: cover;
}
.area{
        height: 80px;
    }
</style>
@endpush

@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script>
    CKEDITOR.replace( 'summaryen', {
    startupFocus: true,
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});
    CKEDITOR.replace('summaryar', {
    startupFocus: true,
    contentsLangDirection: 'rtl',
    filebrowserUploadUrl: "{{route('editor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
    } );

// $.fn.modal.Constructor.prototype._enforceFocus = function() {
//   modal_this = this
//   $(document).on('focusin', function (e) {
//     if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
//     && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select')
//     && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
//       modal_this.$element.focus()
//     }
//   })
// };
</script>
<script>

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
       CKEDITOR.instances.summaryen.on('change', function() {
        if(CKEDITOR.instances.summaryen.getData().length >  0) {
        $('label[for="summaryen"]').hide();
        }
    });
CKEDITOR.instances.summaryar.on('change', function() {
    if(CKEDITOR.instances.summaryar.getData().length >  0) {
     $('label[for="summaryar"]').hide();
    }
});
        $('#testimonial_add_btn').on('click',function(e){
            $('#add-testimonial').trigger("reset")
            CKEDITOR.instances['summaryen'].setData(" ");
            CKEDITOR.instances['summaryar'].setData(" ");
            $('#exampleModalLabel').html('Add Testimonial');
            $('#id_pg').val('');
            $('.scrn-img').attr('src','')
            $('.preview-image-container').attr('style','display:none');
            $('.delete-profile').attr('data-id','');
            
            $('#testimonial-popup').modal({
                show:true
            })
        })

        $('.testimonial_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Testimonial');

            page = $(this).data('id')
            var url = "testimonial/edit/";

            $.get(url  + page, function (data) {

                $('#name_en').val(data.page.lang[0].name);
                $('#name_ar').val(data.page.lang[1].name);
                $('#company_en').val(data.page.lang[0].company);
                $('#company_ar').val(data.page.lang[1].company);
                $('#designation_en').val(data.page.lang[0].designation);
                $('#designation_ar').val(data.page.lang[1].designation);
                $('#id_pg').val(data.page.id)
                CKEDITOR.instances['summaryen'].setData(data.page.lang[0].testimonial);
                CKEDITOR.instances['summaryar'].setData(data.page.lang[1].testimonial);
                if(data.page.image){
                    $('.scrn-img').attr('src',data.page.image)
                    $('.preview-image-container').attr('style','display:block');
                    $('.delete-profile').attr('data-id',data.page.id);
                }else{
                    $('.scrn-img').attr('src','')
                    $('.preview-image-container').attr('style','display:none');
                    $('.delete-profile').attr('data-id','');
                }
                $('#testimonial-popup').modal({
                    show: true
                });
            })
        })

        $('.save_page').on('click',function(e){
            $("#add-testimonial").validate({
                ignore: [],
                rules: {
                name_en  : {
                    required: true,
                },
                name_ar: {
                    required: true,
                },
                designation_en  : {
                    required: true,
                },
                designation_ar: {
                    required: true,
                },
                company_en  : {
                    required: true,
                },
                company_ar: {
                    required: true,
                },
                
              summaryen :{
                    required: function(textarea) {
                     CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                     return editorcontent.length === 0;
                         }

                    },
                summaryar :{
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement();
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                            return editorcontent.length === 0;
                        },
                    },
                },
                messages: {
                name_en: {
                      required:"Name(EN) required",

                      },
                name_ar: {
                      required: "Name (AR) required",
                      },
                designation_en: {
                      required:"Designation(EN) required",

                      },
                designation_ar: {
                      required: "Designation (AR) required",
                      },
                company_en: {
                     required:"Company(EN) required",

                },
                company_ar: {
                      required: "Company(AR) required",
                      },
                summaryen: {
                      required: "Testimonial (EN) required",
                      },
                summaryar: {
                      required: "Testimonial(AR) required",
                      }
                 },
                 errorPlacement: function (error, element) {
                    if (element.attr("type") == "checkbox") {
                        error.insertBefore(element);
                    } else {
                        error.insertAfter(element);
                    }
                },
                 submitHandler: function(form) {
                    $(`#brdar_photo_loader`).show();
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
                    $("input[name='site']:checked").each(function() {
                            test.push($(this).val());
                    });
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('admin.testimonial.store')}}",
                        data:{
                            name_en:$('#name_en').val(),
                            name_ar:$('#name_ar').val(),
                            company_en:$('#company_en').val(),
                            company_ar:$('#company_ar').val(),
                            designation_en:$('#designation_en').val(),
                            designation_ar:$('#designation_ar').val(),
                            testimonial_en: CKEDITOR.instances['summaryen'].getData(),
                            testimonial_ar:CKEDITOR.instances['summaryar'].getData(),
                            id:edit_val

                        },
                        success: function(result){
                            console.log(result.msg)
                            $(`brdar_photo_loader`).hide();
                            $('#testimonial-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'Testimonial updated successfully'
                                });
                                window.location.href = '{{route("admin.testimonial.get")}}';
                         }
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('admin.testimonial.store')}}",
                        data:{
                            name_en:$('#name_en').val(),
                            name_ar:$('#name_ar').val(),
                            company_en:$('#company_en').val(),
                            company_ar:$('#company_ar').val(),
                            designation_en:$('#designation_en').val(),
                            designation_ar:$('#designation_ar').val(),
                            testimonial_en: CKEDITOR.instances['summaryen'].getData(),
                            testimonial_ar:CKEDITOR.instances['summaryar'].getData(),

                        },

                        success: function(result){
                            console.log(result.msg)
                            $(`brdar_photo_loader`).hide();
                            $('#testimonial-popup').modal('hide')
                            if(result.status == 1)
                                   {
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Testimonial added successfully'
                                    });
                                    window.location.href = '{{route("admin.testimonial.get")}}';
                                   }else{
                                    Toast.fire({
                                    icon: 'error',
                                    title: 'some errors'
                                    });
                                   }
                             }
                         }) ;
                    }

                }
            });


        });

        $('.change-status').on('click',function(){

            var id = $(this).data("id");
            var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Testimonial',
            content: 'Are you sure to ' + act_value + ' the Testimonial?',
            buttons: {
                Yes: function() {
            $.ajax({
                        type:"POST",
                        url: "{{route('admin.testimonial.status')}}",
                        data:{
                           status:act_value,
                           id: id

                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.testimonial.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
    $('.testimonial_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Testimonial ? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {

                    $.ajax({
                        url: "{{route('admin.testimonial.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.testimonial.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });

    $('#profile-pic-upload').click(function (e) {
    $('#profile_pic').click();
    });
    
    $('#profile_pic').on('change', function () {
    var id = $('#id_pg').val();
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    var img_type = $(this).data('type');
    const file = $(this)[0].files[0];
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var _URL = window.URL || window.webkitURL;
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
    imgwidth = this.width;
    imgheight = this.height;
    if (imgwidth >= imgw && imgheight >= imgh){
    readUrl(file, img_type, uploadFile, imgw, imgh, id);
    } else{
    $('input[type="file"]').val('');
    Toast.fire({
    icon: 'error',
            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
    });
    }
    }
    });
    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('id_pg', id);
    formData.append('type', type);
    $.ajax({
    type: "POST",
            url: "{{route('testimonial.upload_image')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            if(type == 'image'){
            $(`#profile_pic_loader`).show();
            }
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#id_pg').val(data.id_pg);
            if(data.type == 'image'){
            $('#slider_en_image').html('<div id="profile-pic-upload" class="add">+</div>' +
                    '<div id="profile_pic_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="slider_en_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-profile" data-type="image" data-id="' + data.id_pg + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-profile").click(function(e) {' +
                    'delete_image(' + data.id_pg + ' , "image");' +
                    '});<\/script>');
                    $('body').addClass('modal-open');
                    $('#id_pg').val(data.id_pg);
        }
            $('input[type="file"]').val('');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`profile_pic_loader`).hide();
            }
            }
    });
    }
    
    $('.delete-profile').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });
   
    function delete_image(id, type){
        $.confirm({
            title: '<span class="small">Are you sure to delete this image?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('testimonial.detete_img')}}",
                    data: {
                    id: id,
                    type: type
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    if (data.type == 'image') {
                    $('#slider_en_image').html('<div id="profile-pic-upload" class="add">+</div>' +
                            '<div id="profile_pic_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="slider_en_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="image" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#profile-pic-upload").click(function (e) {' +
                            '$("#profile_pic").click();});<\/script>');
                    }
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    console.log('cancelled');
                    }
            }
    });
    }


</script>
@endpush