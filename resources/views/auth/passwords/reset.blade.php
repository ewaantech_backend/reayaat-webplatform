@extends('layouts.auth')
{{-- @section('flash')
@include('auth.msg')
@endsection --}}
@section('auth-form')

<div class="login-form">
    <h4>RESET PASSWORD</h4>
    <form method="POST" action="{{ route('admin.reset') }}" class="validate_reset_form">
        @csrf
        <input type="hidden" name="email" value="{{$email}}">
        <div class="col-md-12">
            <label>New Password</label>
            <input placeholder="Enter password" id="password" name="password" type="password" class="form-control el"
                required />
            <span class="input-group-text">
                <i id="eye" class="fa fa-eye toggle-password" toggle="#password"></i>
            </span>
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{$message}}</strong>
            </span>
            @enderror
        </div>
        <div class="col-md-12">
            <label>Confirm Password</label>
            <input placeholder="Enter confirm password" id="confirm_password" name="confirm_password" type="password"
                class="form-control el" required />
            <span class="input-group-text">
                <i id="eye-c" class="fa fa-eye toggle-password" toggle="#confirm_password"></i>
            </span>
        </div>
        <div class="col-md-12">

            <button type="submit" class="btn btn-primary btn-flat m-b-15 el" style="background-color: #002e59;">RESET PASSWORD</button>
        </div>
    </form>
</div>
@endsection
@push('css')
<style>
    .sub-title {
        color: #656565;
        margin-bottom: 20px;
    }

    .login-form label {
        text-transform: initial;
    }

    .toggle-password {
        margin-right: 14px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }

    .fa {
        color: black;
        float: right;
    }
</style>
@endpush
@push('scripts')

<script>
    $('.toggle-password').on('click',function(){
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

</script>
<script>
    $('.validate_reset_form').validate({
    rules : {
                password : {
                    required:true,
                    minlength : 5
                },
                confirm_password : {
                    minlength : 5,
                    equalTo :'[name="password"]',
                }
            },
            messages :{
                password:{
                    required:"Password required",
                },
                confirm_password:{
                    equalTo:" Password and confirmation password do not match",
                },

            }

});  

</script>
@endpush