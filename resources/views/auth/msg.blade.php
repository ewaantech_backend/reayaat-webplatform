@if (Session::has('success'))
        <div class="alert alert-success alert-block">
            {{-- <button type="button" class="close" data-dismiss="alert">×</button>     --}}
            <strong>{{ Session::get('success') }}</strong> 
        </div>
        {{ Session::forget('success') }}
        {{ Session::save() }}
@endif
@if ( Session::has('error'))
<div class="alert alert-danger alert-block">
    {{-- <button type="button" class="close" data-dismiss="alert">×</button>     --}}
    <strong>{{ Session::get('error') }}</strong>
</div>
    {{ Session::forget('error') }}
    {{ Session::save() }}
@endif
   
@if (Session::has('warning'))
<div class="alert alert-warning alert-block">
    {{-- <button type="button" class="close" data-dismiss="alert">×</button>     --}}
    <strong>{{  Session::get('warning') }}</strong>


</div>
@endif
   
@if (Session::has('info'))
<div class="alert alert-info alert-block">
    {{-- <button type="button" class="close" data-dismiss="alert">×</button>     --}}
    <strong>{{  Session::get('info') }}</strong>
</div>
@endif
  
{{-- @if ($errors->any())
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    Please check the form below for errors
</div> --}}
{{-- @endif --}}