<!-- Footer -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="p30-15 bbrr" style="padding: 30px 30px; border-radius:0px 0px 26px 26px;" bgcolor="#ffffff">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="text-footer1 pb10" style="color:#000; font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; padding-bottom:10px;">
                <multiline>Copyright © {{date('Y')}} All rights reserved</multiline>
        </td>
    </tr>
</table>
</td>
</tr>
</table>
<!-- END Footer -->
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>