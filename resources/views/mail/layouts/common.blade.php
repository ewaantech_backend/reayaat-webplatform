<table width="100%" cellpadding="10" cellspacing="0" border="0"style="background-color:#eee;color: #404041; font-family:'Muli', Arial,sans-serif;  padding-bottom:10px;">
    <tr>
        <td class="dark-blue-button2 text-button" style="background:#343438; color:#ffffff; font-family:'Muli', Arial,sans-serif; font-size:14px; line-height:18px; padding:12px 30px; text-align:center;  font-weight:bold;">
    <multiline><a href="#" target="_blank" class="link" style="color:#ffffff; text-decoration:none;"><span class="link" style="color:#ffffff; text-decoration:none;">Call us at <b>+966 9373434300</b> or contact at <b>info@kishk.com</b></span></a></multiline>
</td>
</tr>
<tr>
    <td>
        <table width="100%" cellspacing="10" border="0" style="font-size: 13px;color: #404041; " align="center">
            <tr>
                <td width="25%" style="text-align:center;vertical-align:top">
                    <div style="height:42px; padding-bottom:20px;">
                        <img src="{{asset('mail/24-hours.png')}}" width="60px">
                    </div>
                </td>

                <td width="25%" style="text-align:center;vertical-align:top">
                    <div style="height:42px; padding-bottom:20px;">
                        <img src="{{asset('mail/img_458847.png')}}"  width="60px" >
                    </div>
                </td>
                <td width="25%" style="text-align:center;vertical-align:top">
                    <div style="height:42px; padding-bottom:20px;">
                        <img src="{{asset('mail/easy-return.png')}}" width="65px" >
                    </div>
                </td>
            </tr>
            <tr>
                <td width="25%" style="text-align:center;vertical-align:top">
                    <div style="line-height:22px;text-align:center ;
                         font-weight: 700;">
                        CUSTOMER SERVICE
                    </div>
                </td>

                <td width="25%" style="text-align:center;vertical-align:top">
                    <div style="line-height:22px;text-align:center;
                         font-weight: 700;">
                        SATISFACTION GUARANTEED
                    </div>
                </td>
                <td width="25%" style="text-align:center;vertical-align:top">
                    <div style="line-height:22px;text-align:center;
                         font-weight: 700;">
                        HASSLE-FREE RETURNS
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>