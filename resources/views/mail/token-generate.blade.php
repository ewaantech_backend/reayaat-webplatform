@include('mail.layouts.header')
<repeater>
    <!-- Intro -->
    <layout label='Intro'>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="padding-bottom: 10px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="tbrr p30-15" style="padding: 40px 30px; border-radius:26px 26px 0px 0px;" bgcolor="#ffffff">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="img m-center" style="font-size:0pt; line-height:30pt;padding-bottom:30px; text-align:center;"><img src="{{asset('assets/images/logo.png')}}" width="142" height="70" editable="true" border="0" alt=""  style="    border-radius: 6px;"></td>
                                    </tr>
                                    <tr>
                                        <td class="h1 pb25" style="color:#db0000; font-family:'Muli', Arial,sans-serif; font-size:30px; line-height:20px; text-align:center; padding-bottom:10px; margin-bottom:5px;">
                                    <multiline>
                                        Hello!
                                        </td>
                                        </tr>
                                            <tr>
                                                <td class="text-center pb25" style="color:#00000; font-family:'Muli', Arial,sans-serif; font-size:16px;  text-align:center; padding-bottom:10px; line-height:20px; margin-bottom:5px; ">
                                            <multiline><b>Here is your code for resetting your password </b></multiline>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center pb25" style="color:#00000; font-family:'Muli', Arial,sans-serif; font-size:16px;  text-align:center; padding-bottom:20px; padding-top:15px; line-height:20px; margin-bottom:5px; ">
                                            <multiline> <span style="color: #db0000; font-size: 18px; border: 2px solid #9E9E9E; padding: 5px 18px 5px 19px;">{{$token}}</span></b></multiline>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center pb25" style="color:#00000; font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:20px; margin-bottom:10px; text-align:center; padding-bottom:10px;">
                                            <multiline>If you did not initiate this request, you can safely ignore this mail</multiline>
                                            </td>
                                            </tr>
                                            </table>
                                            </td>
                                            </tr>
                                            </table>
                                            </td>
                                            </tr>
                                            </table>
                                            </layout>
                                            <!-- END Two Columns -->
                                            </repeater>
                                            @include('mail.layouts.footer')