<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\AdminVerificationController;
use App\Http\Controllers\Admin\ProfileDetailsController;
use App\Http\Controllers\Admin\OtherSettingsController;
use App\Http\Controllers\Admin\FAQController;
use App\Http\Controllers\Admin\LogController;
use App\Http\Controllers\Admin\UserManagementController;
use App\Http\Controllers\Admin\OpportunityController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TypeController;
use App\Http\Controllers\Admin\TopicController;
use App\Http\Controllers\Admin\SlideController;
use App\Http\Controllers\Admin\FeatureController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\PagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
//Auth::routes(['register' => false]);
Route::post("login", [LoginController::class, 'login'])->name('login');
Route::post("token/send", [AdminVerificationController::class, 'resetPasswordToken'])->name('admin.verify');
Route::post("token/verify", [AdminVerificationController::class, 'tokenVerify'])->name('token.verify');
Route::post("admin/password/change", [AdminVerificationController::class, 'passwordUpdating'])->name('admin.reset');

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::group(['prefix' => 'admin', 'middleware' => 'auth:web'], function () {
    Route::get("logout", [AdminVerificationController::class, 'logout'])->name('admin.logout');

    Route::get("/dashboard", [DashboardController::class, 'get'])->name('dashboard.get');

    Route::get("/edit-profile", [ProfileDetailsController::class, 'edit'])->name('profile.edit');
    Route::post("/update-details", [ProfileDetailsController::class, 'updateDetails'])->name('update.details');
    Route::post("/password-change", [ProfileDetailsController::class, 'updatePassword'])->name('admin.pass.reset');

    // Logs

    Route::get('/logs', [LogController::class, 'index'])->name('logs');

    //  Faq
    Route::get("/faq", [FAQController::class, 'get'])->name('admin.faq.get');
    Route::post("/faq/store", [FAQController::class, 'store'])->name('admin.faq.store');
    Route::post("/faq/status", [FAQController::class, 'statusUpdate'])->name('admin.faq.status');
    Route::get("/faq/edit/{id}", [FAQController::class, 'edit'])->name('admin.faq.edit');
    Route::post("/faq/update", [FAQController::class, 'update'])->name('admin.faq.update');
    Route::post("/faq/delete", [FAQController::class, 'destroy'])->name('admin.faq.delete');
    //Route::post("/faq/search", [FAQController::class, 'search'])->name('admin.faq.search');

    // Common 
    Route::post('ckeditor/image_upload', [CKEditorUpload::class, 'upload'])->name('editor.upload');

    //  Other settings
    Route::get("/other-settings", [OtherSettingsController::class, 'get'])->name('admin.other.get');
    Route::post("/other-settings/store", [OtherSettingsController::class, 'store'])->name('admin.other.store');

    //Admin User 
    Route::get("/users", [UserManagementController::class, 'get'])->name('admin.user.get');
    Route::post("/user/store", [UserManagementController::class, 'store'])->name('admin.user.store');
    Route::post("/user/status-change", [UserManagementController::class, 'updateStatus'])->name('admin.user.status-change');
    Route::get("user/edit/{id}", [UserManagementController::class, 'edit'])->name('admin.user.edit');
    Route::post("/user/update", [UserManagementController::class, 'update'])->name('admin.user.update');
    Route::post("/user/delete", [UserManagementController::class, 'destroy'])->name('admin.user.delete');
    Route::post("/user/password-send", [UserManagementController::class, 'sendPassword'])->name('admin.user.password.send');

    //  Opportunity per year
    Route::get("/opportunity", [OpportunityController::class, 'get'])->name('admin.opportunity.get');
    Route::get("opportunity/edit/{id}", [OpportunityController::class, 'edit'])->name('admin.opportunity.edit');
    Route::post("opportunity/update", [OpportunityController::class, 'update'])->name('admin.opportunity.update');
    Route::post("/opportunity/status", [OpportunityController::class, 'statusUpdate'])->name('admin.opportunity.status');

    //  Category
    Route::get("/category", [CategoryController::class, 'get'])->name('admin.category.get');
    Route::post("/category/store", [CategoryController::class, 'store'])->name('admin.category.store');
    Route::get("category/edit/{id}", [CategoryController::class, 'edit'])->name('admin.category.edit');
    Route::post("/category/delete", [CategoryController::class, 'destroy'])->name('admin.category.delete');
    Route::post("/category/status-change", [CategoryController::class, 'updateStatus'])->name('admin.category.status-change');

    //  Type
    Route::get("/type", [TypeController::class, 'get'])->name('admin.type.get');
    Route::post("/type/store", [TypeController::class, 'store'])->name('admin.type.store');
    Route::get("type/edit/{id}", [TypeController::class, 'edit'])->name('admin.type.edit');
    Route::post("/type/delete", [TypeController::class, 'destroy'])->name('admin.type.delete');
    Route::post("/type/status-change", [TypeController::class, 'updateStatus'])->name('admin.type.status-change');

    //  Topic
    Route::get("/topic", [TopicController::class, 'get'])->name('admin.topic.get');
    Route::post("/topic/store", [TopicController::class, 'store'])->name('admin.topic.store');
    Route::get("topic/edit/{id}", [TopicController::class, 'edit'])->name('admin.topic.edit');
    Route::post("/topic/delete", [TopicController::class, 'destroy'])->name('admin.topic.delete');
    Route::post("/topic/status-change", [TopicController::class, 'updateStatus'])->name('admin.topic.status-change');

    //  Slide
    Route::get("/slide", [SlideController::class, 'index'])->name('admin.slide.get');
    Route::get('/slide/create', [SlideController::class, 'create'])->name('admin.create_slider');
    Route::post('/slide/save', [SlideController::class, 'save'])->name('admin.save_slider');
    Route::post('/slide/activate', [SlideController::class, 'changeStatus'])->name('slider.activate');
    Route::post('/slide/upload_image', [SlideController::class, 'upload_image'])->name('slider.upload_image');
    Route::post('/slide/detete_img', [SlideController::class, 'detete_img'])->name('slider.detete_img');
    Route::post("/slide/delete", [SlideController::class, 'destroy'])->name('admin.slide.delete');

    //  Feature
    Route::get("/feature", [FeatureController::class, 'get'])->name('admin.feature.get');
    Route::get("feature/edit/{id}", [FeatureController::class, 'edit'])->name('admin.feature.edit');
    Route::post("feature/update", [FeatureController::class, 'update'])->name('admin.feature.update');
    Route::post("/feature/status", [FeatureController::class, 'statusUpdate'])->name('admin.feature.status');

    //  Testimonial
    Route::get("/testimonial", [TestimonialController::class, 'get'])->name('admin.testimonial.get');
    Route::post("/testimonial/store", [TestimonialController::class, 'store'])->name('admin.testimonial.store');
    Route::get("/testimonial/edit/{id}", [TestimonialController::class, 'edit'])->name('admin.testimonial.edit');
    Route::post("/testimonial/status", [TestimonialController::class, 'statusUpdate'])->name('admin.testimonial.status');
    Route::post("/testimonial/delete", [TestimonialController::class, 'destroy'])->name('admin.testimonial.delete');
    Route::post('/testimonial/upload_image', [TestimonialController::class, 'upload_image'])->name('testimonial.upload_image');
    Route::post('/testimonial/detete_img', [TestimonialController::class, 'detete_img'])->name('testimonial.detete_img');

    //  Pages
    Route::get("/pages", [PagesController::class, 'get'])->name('admin.pages.get');
    Route::get('/pages/create', [PagesController::class, 'create'])->name('pages.create');

    Route::get('/pages/about_tabs', [PagesController::class, 'about_tabs'])->name('about_tabs');
    Route::post("/pages/aboutus/pageinfo", [PagesController::class, 'about_pageinfo'])->name('pages.aboutus.pageinfo');
    Route::post("/pages/aboutus/seo", [PagesController::class, 'about_seo'])->name('pages.aboutus.seo');
    Route::post('/aboutus/upload_image', [PagesController::class, 'upload_image'])->name('aboutus.upload_image');
    Route::post('/aboutus/detete_img', [PagesController::class, 'detete_img'])->name('aboutus.detete_img');

    Route::get('/pages/terms_tabs', [PagesController::class, 'terms_tabs'])->name('terms_tabs');
    Route::post("/pages/terms/pageinfo", [PagesController::class, 'terms_pageinfo'])->name('pages.terms.pageinfo');
    Route::post("/pages/terms/seo", [PagesController::class, 'terms_seo'])->name('pages.terms.seo');

    Route::get('/pages/privacy_tabs', [PagesController::class, 'privacy_tabs'])->name('privacy_tabs');
    Route::post("/pages/privacy/pageinfo", [PagesController::class, 'privacy_pageinfo'])->name('pages.privacy.pageinfo');
    Route::post("/pages/privacy/seo", [PagesController::class, 'privacy_seo'])->name('pages.privacy.seo');

    Route::get('/pages/contact', [PagesController::class, 'contact_tabs'])->name('contact_tabs');
    Route::post("/pages/contact/pageinfo", [PagesController::class, 'contact_pageinfo'])->name('pages.contact.pageinfo');
    Route::post("/pages/contact/seo", [PagesController::class, 'contact_seo'])->name('pages.contact.seo');

    Route::get('/pages/sponsor', [PagesController::class, 'sponsor_tabs'])->name('sponsor_tabs');
    Route::post("/pages/sponsor/showcase_program", [PagesController::class, 'sponsor_showcase_program'])->name('pages.sponsor.showcase_program');
    Route::post("/pages/sponsor/sponsor_whatinit", [PagesController::class, 'sponsor_whatinit'])->name('pages.sponsor.whatinit');
    Route::post("/pages/sponsor/seo", [PagesController::class, 'sponsor_seo'])->name('pages.sponsor.seo');
    Route::post('/sponsor/upload_image', [PagesController::class, 'sponsor_upload_image'])->name('sponsor.upload_image');
    Route::post('/sponsor/detete_img', [PagesController::class, 'sponsor_detete_img'])->name('sponsor.detete_img');

});
